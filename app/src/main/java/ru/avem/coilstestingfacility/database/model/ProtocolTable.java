package ru.avem.coilstestingfacility.database.model;

public class ProtocolTable {
    static final String TABLE_NAME = "protocols";

    public static class COLUMN {
        static final String ID = "_id";
        static final String SERIAL_NUMBER = "serialNumber";
        static final String SUBJECT_NAME = "subjectName";
        static final String INDUCTOR_NAME = "inductorName";
        static final String SPECIFIED_U_TC = "specifiedUTC";
        static final String SPECIFIED_U_TC_MINUS = "specifiedUTCMinus";
        static final String SPECIFIED_U_TC_PLUS = "specifiedUTCPlus";
        static final String OBJECT_U_TC = "objectUTC";
        static final String SPECIFIED_I_TC = "specifiedITC";
        static final String SPECIFIED_I_TC_MINUS = "specifiedITCMinus";
        static final String SPECIFIED_I_TC_PLUS = "specifiedITCPlus";
        static final String OBJECT_I_TC = "objectITC";
        static final String RESULT_TC = "resultTC";
        static final String SPECIFIED_U_ES = "specifiedUES";
        static final String SPECIFIED_U_ES_MINUS = "specifiedUESMinus";
        static final String SPECIFIED_U_ES_PLUS = "specifiedUESPlus";
        static final String OBJECT_U_ES = "objectUES";
        static final String OBJECT_I_ES = "objectIES";
        static final String RESULT_ES = "resultES";
        static final String SPECIFIED_R = "specifiedR";
        static final String SPECIFIED_R_MINUS = "specifiedRMinus";
        static final String SPECIFIED_R_PLUS = "specifiedRPlus";
        static final String EXPERIMENT_TIME = "experimentTime";
        static final String OBJECT_R = "objectR";
        static final String RESULT_R = "resultR";
        static final String POSITION1 = "position1";
        static final String POSITION1_NUMBER = "position1Number";
        static final String POSITION1_FULL_NAME = "position1FullName";
        static final String POSITION2 = "position2";
        static final String POSITION2_NUMBER = "position2Number";
        static final String POSITION2_FULL_NAME = "position2FullName";
        static final String DATE = "date";
    }

    static final String CREATE_SCRIPT =
            String.format("CREATE TABLE %s ("
                            + "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s INTEGER,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s INTEGER,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s INTEGER,"
                            + "%s REAL,"
                            + "%s INTEGER,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s TEXT,"
                            + "%s INTEGER" + ");",
                    TABLE_NAME,
                    COLUMN.ID,
                    COLUMN.SERIAL_NUMBER,
                    COLUMN.SUBJECT_NAME,
                    COLUMN.INDUCTOR_NAME,
                    COLUMN.SPECIFIED_U_TC,
                    COLUMN.SPECIFIED_U_TC_MINUS,
                    COLUMN.SPECIFIED_U_TC_PLUS,
                    COLUMN.OBJECT_U_TC,
                    COLUMN.SPECIFIED_I_TC,
                    COLUMN.SPECIFIED_I_TC_MINUS,
                    COLUMN.SPECIFIED_I_TC_PLUS,
                    COLUMN.OBJECT_I_TC,
                    COLUMN.RESULT_TC,
                    COLUMN.SPECIFIED_U_ES,
                    COLUMN.SPECIFIED_U_ES_MINUS,
                    COLUMN.SPECIFIED_U_ES_PLUS,
                    COLUMN.OBJECT_U_ES,
                    COLUMN.OBJECT_I_ES,
                    COLUMN.RESULT_ES,
                    COLUMN.SPECIFIED_R,
                    COLUMN.SPECIFIED_R_MINUS,
                    COLUMN.SPECIFIED_R_PLUS,
                    COLUMN.EXPERIMENT_TIME,
                    COLUMN.OBJECT_R,
                    COLUMN.RESULT_R,
                    COLUMN.POSITION1,
                    COLUMN.POSITION1_NUMBER,
                    COLUMN.POSITION1_FULL_NAME,
                    COLUMN.POSITION2,
                    COLUMN.POSITION2_NUMBER,
                    COLUMN.POSITION2_FULL_NAME,
                    COLUMN.DATE);
}