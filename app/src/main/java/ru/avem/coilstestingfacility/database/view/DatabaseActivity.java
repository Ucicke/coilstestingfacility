package ru.avem.coilstestingfacility.database.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.avem.coilstestingfacility.R;
import ru.avem.coilstestingfacility.database.model.DatabaseAdapter;
import ru.avem.coilstestingfacility.database.model.Inductor;
import ru.avem.coilstestingfacility.database.model.Subject;

import static ru.avem.coilstestingfacility.R.id.objects_selector;

public class DatabaseActivity extends AppCompatActivity {
    @BindView(objects_selector)
    Spinner mSubjects;
    @BindView(R.id.inductors_selector)
    Spinner mInductors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        ButterKnife.bind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        DatabaseAdapter adapter = new DatabaseAdapter(this);
        adapter.open();
        ArrayAdapter<Subject> subjectArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, adapter.getSubjects());
        subjectArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSubjects.setAdapter(subjectArrayAdapter);
        ArrayAdapter<Inductor> inductorArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, adapter.getInductors());
        inductorArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mInductors.setAdapter(inductorArrayAdapter);
        adapter.close();
    }

    @OnClick({R.id.add_subject, R.id.edit_subject, R.id.include, R.id.edit_inductor})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.add_subject:
                intent = new Intent(getApplicationContext(), SubjectActivity.class);
                startActivity(intent);
                break;
            case R.id.edit_subject:
                intent = new Intent(getApplicationContext(), SubjectActivity.class);
                intent.putExtra("id", ((Subject) mSubjects.getSelectedItem()).getId());
                startActivity(intent);
                break;
            case R.id.include:
                intent = new Intent(getApplicationContext(), InductorActivity.class);
                startActivity(intent);
                break;
            case R.id.edit_inductor:
                intent = new Intent(getApplicationContext(), InductorActivity.class);
                intent.putExtra("id", ((Inductor) mInductors.getSelectedItem()).getId());
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }
}
