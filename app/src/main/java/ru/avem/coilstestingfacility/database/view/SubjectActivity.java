package ru.avem.coilstestingfacility.database.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.avem.coilstestingfacility.R;
import ru.avem.coilstestingfacility.database.model.DatabaseAdapter;
import ru.avem.coilstestingfacility.database.model.Inductor;
import ru.avem.coilstestingfacility.database.model.Subject;


public class SubjectActivity extends AppCompatActivity {

    @BindView(R.id.name)
    EditText mName;
    @BindView(R.id.included)
    Spinner mIncluded;
    @BindView(R.id.all)
    Spinner mAll;
    @BindView(R.id.delete)
    Button mDeleteButton;
    private DatabaseAdapter adapter;
    private long subjectId = 0;

    private ArrayAdapter<Inductor> mIncludedInductorArrayAdapter;
    private ArrayAdapter<Inductor> mAllInductorArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        ButterKnife.bind(this);

        adapter = new DatabaseAdapter(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            subjectId = extras.getLong("id");
        }
        adapter.open();
        if (subjectId > 0) {
            Subject subject = adapter.getSubject(subjectId);
            mName.setText(subject.getName());
            List<Inductor> includedInductors = subject.getInductors();
            mIncludedInductorArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, includedInductors);
            List<Inductor> allInductors = adapter.getInductors();
            for (Inductor oneOfIncludedInductor : includedInductors) {
                for (Inductor oneOfAllInductor : allInductors) {
                    if (oneOfAllInductor.getName().equals(oneOfIncludedInductor.getName())) {
                        allInductors.remove(oneOfAllInductor);
                        break;
                    }
                }
            }
            mAllInductorArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, allInductors);
        } else {
            mDeleteButton.setVisibility(View.GONE);
            mIncludedInductorArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<Inductor>());
            mAllInductorArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, adapter.getInductors());
        }
        adapter.close();
        mIncludedInductorArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mIncluded.setAdapter(mIncludedInductorArrayAdapter);
        mAllInductorArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAll.setAdapter(mAllInductorArrayAdapter);
    }

    @OnClick({R.id.exclude, R.id.include, R.id.save, R.id.delete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exclude:
                exclude();
                break;
            case R.id.include:
                include();
                break;
            case R.id.save:
                save();
                break;
            case R.id.delete:
                delete();
                break;
        }
    }

    private void exclude() {
        if (mIncluded.getCount() > 0) {
            Inductor toExclude = (Inductor) mIncluded.getSelectedItem();
            mAllInductorArrayAdapter.add(toExclude);
            mIncludedInductorArrayAdapter.remove(toExclude);
        }
    }

    private void include() {
        if (mAll.getCount() > 0) {
            Inductor toInclude = (Inductor) mAll.getSelectedItem();
            mIncludedInductorArrayAdapter.add(toInclude);
            mAllInductorArrayAdapter.remove(toInclude);
        }
    }

    private void save() {
        if (mName.getText().length() > 0 && mIncluded.getCount() > 0) {
            String name = mName.getText().toString();
            List<Inductor> inductors = new ArrayList<>();
            for (int i = 0; i < mIncludedInductorArrayAdapter.getCount(); i++) {
                inductors.add(mIncludedInductorArrayAdapter.getItem(i));
            }
            Subject subject = new Subject(subjectId, name, inductors);

            adapter.open();
            if (subjectId > 0) {
                adapter.updateSubject(subject);
            } else {
                adapter.insertSubject(subject);
            }
            adapter.close();
            goHome();
        } else {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show();
        }
    }

    private void delete() {
        adapter.open();
        adapter.deleteSubject(subjectId);
        adapter.close();
        goHome();
    }

    private void goHome() {
        Intent intent = new Intent(this, DatabaseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}