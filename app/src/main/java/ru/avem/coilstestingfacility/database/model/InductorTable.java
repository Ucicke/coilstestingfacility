package ru.avem.coilstestingfacility.database.model;

public class InductorTable {
    static final String TABLE_NAME = "inductors";

    public static class COLUMN {
        static final String ID = "_id";
        static final String NAME = "name";
        static final String SPECIFIED_U_TC = "specifiedUTC";
        static final String SPECIFIED_U_TC_Minus = "specifiedUTCMinus";
        static final String SPECIFIED_U_TC_Plus = "specifiedUTCPlus";
        static final String SPECIFIED_I_TC = "specifiedITC";
        static final String SPECIFIED_I_TC_Minus = "specifiedITCMinus";
        static final String SPECIFIED_I_TC_Plus = "specifiedITCPlus";
        static final String SPECIFIED_U_ES = "specifiedUES";
        static final String SPECIFIED_U_ES_Minus = "specifiedUESMinus";
        static final String SPECIFIED_U_ES_Plus = "specifiedUESPlus";
        static final String SPECIFIED_R = "specifiedR";
        static final String SPECIFIED_R_Minus = "specifiedRMinus";
        static final String SPECIFIED_R_Plus = "specifiedRPlus";
        static final String EXPERIMENT_TIME = "experimentTime";
    }

    static final String CREATE_SCRIPT =
            String.format("CREATE TABLE %s ("
                            + "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
                            + "%s TEXT,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s REAL,"
                            + "%s INTEGER" + ");",
                    TABLE_NAME,
                    COLUMN.ID,
                    COLUMN.NAME,
                    COLUMN.SPECIFIED_U_TC,
                    COLUMN.SPECIFIED_U_TC_Minus,
                    COLUMN.SPECIFIED_U_TC_Plus,
                    COLUMN.SPECIFIED_I_TC,
                    COLUMN.SPECIFIED_I_TC_Minus,
                    COLUMN.SPECIFIED_I_TC_Plus,
                    COLUMN.SPECIFIED_U_ES,
                    COLUMN.SPECIFIED_U_ES_Minus,
                    COLUMN.SPECIFIED_U_ES_Plus,
                    COLUMN.SPECIFIED_R,
                    COLUMN.SPECIFIED_R_Minus,
                    COLUMN.SPECIFIED_R_Plus,
                    COLUMN.EXPERIMENT_TIME);

    static String formInsertScript(String name, String specifiedUTC, String specifiedUTCMinus, String specifiedUTCPlus,
                                   String specifiedITC, String specifiedITCMinus, String specifiedITCPlus,
                                   String specifiedUES, String specifiedUESMinus, String specifiedUESPlus, String specifiedR,
                                   String specifiedRMinus, String specifiedRPlus, String experimentTime) {
        return String.format("INSERT INTO %s ("
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s,"
                        + "%s) "
                        + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'" + ");",
                TABLE_NAME,
                COLUMN.NAME,
                COLUMN.SPECIFIED_U_TC,
                COLUMN.SPECIFIED_U_TC_Minus,
                COLUMN.SPECIFIED_U_TC_Plus,
                COLUMN.SPECIFIED_I_TC,
                COLUMN.SPECIFIED_I_TC_Minus,
                COLUMN.SPECIFIED_I_TC_Plus,
                COLUMN.SPECIFIED_U_ES,
                COLUMN.SPECIFIED_U_ES_Minus,
                COLUMN.SPECIFIED_U_ES_Plus,
                COLUMN.SPECIFIED_R,
                COLUMN.SPECIFIED_R_Minus,
                COLUMN.SPECIFIED_R_Plus,
                COLUMN.EXPERIMENT_TIME,
                name,
                specifiedUTC,
                specifiedUTCMinus,
                specifiedUTCPlus,
                specifiedITC,
                specifiedITCMinus,
                specifiedITCPlus,
                specifiedUES,
                specifiedUESMinus,
                specifiedUESPlus,
                specifiedR,
                specifiedRMinus,
                specifiedRPlus,
                experimentTime);
    }
}