package ru.avem.coilstestingfacility.database.model;

import java.text.SimpleDateFormat;

public class Protocol {
    private long mId;
    private String mSerialNumber;
    private String mSubjectName;
    private String mInductorName;
    private double mSpecifiedUTC;
    private double mSpecifiedUTCMinus;
    private double mSpecifiedUTCPlus;
    private double mObjectUTC;
    private double mSpecifiedITC;
    private double mSpecifiedITCMinus;
    private double mSpecifiedITCPlus;
    private double mObjectITC;
    private boolean mResultTC;
    private double mSpecifiedUES;
    private double mSpecifiedUESMinus;
    private double mSpecifiedUESPlus;
    private double mObjectUES;
    private double mObjectIES;
    private boolean mResultES;
    private double mSpecifiedR;
    private double mSpecifiedRMinus;
    private double mSpecifiedRPlus;
    private int mExperimentTime;
    private double mObjectR;
    private boolean mResultR;
    private String mPosition1;
    private String mPosition1Number;
    private String mPosition1FullName;
    private String mPosition2;
    private String mPosition2Number;
    private String mPosition2FullName;
    private long mDate;

    public Protocol(String serialNumber) {
        this(0, serialNumber, "", "",
                -1, -1, -1,
                -1,
                -1, -1, -1,
                -1,
                false,
                -1, -1, -1,
                -1,
                -1,
                false,
                -1, -1, -1,
                -1,
                -1,
                false,
                null, null, null,
                null, null, null,
                System.currentTimeMillis());
    }

    public Protocol(long id, String serialNumber, String subjectName, String inductorName,
                    double specifiedUTC, double specifiedUTCMinus, double specifiedUTCPlus,
                    double objectUTC,
                    double specifiedITC, double specifiedITCMinus, double specifiedITCPlus,
                    double objectITC,
                    boolean resultTC,
                    double specifiedUES, double specifiedUESMinus, double specifiedUESPlus,
                    double objectUES,
                    double objectIES,
                    boolean resultES,
                    double specifiedR, double specifiedRMinus, double specifiedRPlus,
                    int experimentTime,
                    double objectR,
                    boolean resultR,
                    String position1, String position1Number, String position1FullName,
                    String position2, String position2Number, String position2FullName,
                    long date) {
        mId = id;
        mSerialNumber = serialNumber;
        mSubjectName = subjectName;
        mInductorName = inductorName;
        mSpecifiedUTC = specifiedUTC;
        mSpecifiedUTCMinus = specifiedUTCMinus;
        mSpecifiedUTCPlus = specifiedUTCPlus;
        mObjectUTC = objectUTC;
        mSpecifiedITC = specifiedITC;
        mSpecifiedITCMinus = specifiedITCMinus;
        mSpecifiedITCPlus = specifiedITCPlus;
        mObjectITC = objectITC;
        mResultTC = resultTC;
        mSpecifiedUES = specifiedUES;
        mSpecifiedUESMinus = specifiedUESMinus;
        mSpecifiedUESPlus = specifiedUESPlus;
        mObjectUES = objectUES;
        mObjectIES = objectIES;
        mResultES = resultES;
        mSpecifiedR = specifiedR;
        mSpecifiedRMinus = specifiedRMinus;
        mSpecifiedRPlus = specifiedRPlus;
        mExperimentTime = experimentTime;
        mObjectR = objectR;
        mResultR = resultR;
        mPosition1 = position1;
        mPosition1Number = position1Number;
        mPosition1FullName = position1FullName;
        mPosition2 = position2;
        mPosition2Number = position2Number;
        mPosition2FullName = position2FullName;
        mDate = date;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public String getSubjectName() {
        return mSubjectName;
    }

    public void setSubjectName(String subjectName) {
        mSubjectName = subjectName;
    }

    public String getInductorName() {
        return mInductorName;
    }

    public void setInductorName(String inductorName) {
        mInductorName = inductorName;
    }

    public double getSpecifiedUTC() {
        return mSpecifiedUTC;
    }

    public void setSpecifiedUTC(double specifiedUTC) {
        mSpecifiedUTC = specifiedUTC;
    }

    public double getSpecifiedUTCMinus() {
        return mSpecifiedUTCMinus;
    }

    public void setSpecifiedUTCMinus(double specifiedUTCMinus) {
        mSpecifiedUTCMinus = specifiedUTCMinus;
    }

    public double getSpecifiedUTCPlus() {
        return mSpecifiedUTCPlus;
    }

    public void setSpecifiedUTCPlus(double specifiedUTCPlus) {
        mSpecifiedUTCPlus = specifiedUTCPlus;
    }

    public double getObjectUTC() {
        return mObjectUTC;
    }

    public void setObjectUTC(double objectUTC) {
        mObjectUTC = objectUTC;
    }

    public double getSpecifiedITC() {
        return mSpecifiedITC;
    }

    public void setSpecifiedITC(double specifiedITC) {
        mSpecifiedITC = specifiedITC;
    }

    public double getSpecifiedITCMinus() {
        return mSpecifiedITCMinus;
    }

    public void setSpecifiedITCMinus(double specifiedITCMinus) {
        mSpecifiedITCMinus = specifiedITCMinus;
    }

    public double getSpecifiedITCPlus() {
        return mSpecifiedITCPlus;
    }

    public void setSpecifiedITCPlus(double specifiedITCPlus) {
        mSpecifiedITCPlus = specifiedITCPlus;
    }

    public double getObjectITC() {
        return mObjectITC;
    }

    public void setObjectITC(double objectITC) {
        mObjectITC = objectITC;
    }

    public boolean isResultTC() {
        return mResultTC;
    }

    public void setResultTC(boolean resultTC) {
        mResultTC = resultTC;
    }

    public double getSpecifiedUES() {
        return mSpecifiedUES;
    }

    public void setSpecifiedUES(double specifiedUES) {
        mSpecifiedUES = specifiedUES;
    }

    public double getSpecifiedUESMinus() {
        return mSpecifiedUESMinus;
    }

    public void setSpecifiedUESMinus(double specifiedUESMinus) {
        mSpecifiedUESMinus = specifiedUESMinus;
    }

    public double getSpecifiedUESPlus() {
        return mSpecifiedUESPlus;
    }

    public void setSpecifiedUESPlus(double specifiedUESPlus) {
        mSpecifiedUESPlus = specifiedUESPlus;
    }

    public double getObjectUES() {
        return mObjectUES;
    }

    public void setObjectUES(double objectUES) {
        mObjectUES = objectUES;
    }

    public double getObjectIES() {
        return mObjectIES;
    }

    public void setObjectIES(double objectIES) {
        mObjectIES = objectIES;
    }

    public boolean isResultES() {
        return mResultES;
    }

    public void setResultES(boolean resultES) {
        mResultES = resultES;
    }

    public double getSpecifiedR() {
        return mSpecifiedR;
    }

    public void setSpecifiedR(double specifiedR) {
        mSpecifiedR = specifiedR;
    }

    public double getSpecifiedRMinus() {
        return mSpecifiedRMinus;
    }

    public void setSpecifiedRMinus(double specifiedRMinus) {
        mSpecifiedRMinus = specifiedRMinus;
    }

    public double getSpecifiedRPlus() {
        return mSpecifiedRPlus;
    }

    public void setSpecifiedRPlus(double specifiedRPlus) {
        mSpecifiedRPlus = specifiedRPlus;
    }

    public int getExperimentTime() {
        return mExperimentTime;
    }

    public void setExperimentTime(int experimentTime) {
        mExperimentTime = experimentTime;
    }

    public double getObjectR() {
        return mObjectR;
    }

    public void setObjectR(double objectR) {
        mObjectR = objectR;
    }

    public boolean isResultR() {
        return mResultR;
    }

    public void setResultR(boolean resultR) {
        mResultR = resultR;
    }

    public String getPosition1() {
        return mPosition1;
    }

    public void setPosition1(String position1) {
        mPosition1 = position1;
    }

    public String getPosition1Number() {
        return mPosition1Number;
    }

    public void setPosition1Number(String position1Number) {
        mPosition1Number = position1Number;
    }

    public String getPosition1FullName() {
        return mPosition1FullName;
    }

    public void setPosition1FullName(String position1FullName) {
        mPosition1FullName = position1FullName;
    }

    public String getPosition2() {
        return mPosition2;
    }

    public void setPosition2(String position2) {
        mPosition2 = position2;
    }

    public String getPosition2Number() {
        return mPosition2Number;
    }

    public void setPosition2Number(String position2Number) {
        mPosition2Number = position2Number;
    }

    public String getPosition2FullName() {
        return mPosition2FullName;
    }

    public void setPosition2FullName(String position2FullName) {
        mPosition2FullName = position2FullName;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("Дата: dd-MM-yy Время: HH:mm:ss");
        return String.format("%s. № %s (Объект: %s(%s)) %s", mId, mSerialNumber, mSubjectName, mInductorName, sdf.format(mDate));
    }
}
