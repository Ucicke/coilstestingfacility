package ru.avem.coilstestingfacility.database.model;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private long mId;
    private String mName;
    private List<Inductor> mInductors;

    public Subject(long id, String name, List<Inductor> inductors) {
        mId = id;
        mName = name;
        mInductors = inductors;
    }

    public long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<Inductor> getInductors() {
        return mInductors;
    }

    public void setInductors(List<Inductor> inductors) {
        mInductors = inductors;
    }

    @Override
    public String toString() {
        return mName;
    }

    static List<Inductor> stringToList(String inductors, DatabaseAdapter adapter) {
        String[] namesOfInductors = inductors.split(",");
        List<Inductor> listOfInductors = new ArrayList<>();
        for (String nameOfInductor : namesOfInductors) {
            listOfInductors.add(adapter.getInductor(nameOfInductor));
        }
        return listOfInductors;
    }

    static String listToString(List<Inductor> inductors) {
        StringBuilder builder = new StringBuilder();
        String prefix = "";
        for (Inductor inductor : inductors) {
            builder.append(prefix);
            prefix = ",";
            builder.append(inductor.getName());
        }
        return builder.toString();
    }
}