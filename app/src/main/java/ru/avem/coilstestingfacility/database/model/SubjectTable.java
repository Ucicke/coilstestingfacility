package ru.avem.coilstestingfacility.database.model;

class SubjectTable {
    static final String TABLE_NAME = "subjects";

    public static class COLUMN {
        static final String ID = "_id";
        static final String NAME = "name";
        static final String INDUCTORS = "inductors";
    }

    static final String CREATE_SCRIPT =
            String.format("CREATE TABLE %s ("
                            + "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
                            + "%s TEXT,"
                            + "%s TEXT" + ");",
                    TABLE_NAME,
                    COLUMN.ID,
                    COLUMN.NAME,
                    COLUMN.INDUCTORS);

    static String formInsertScript(String name, String inductors) {
        return String.format("INSERT INTO %s ("
                        + "%s,"
                        + "%s) "
                        + "VALUES ('%s', '%s'" + ");",
                TABLE_NAME,
                COLUMN.NAME,
                COLUMN.INDUCTORS,
                name,
                inductors);
    }
}