package ru.avem.coilstestingfacility.database.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DatabaseHelper extends SQLiteOpenHelper {
    private static final String NAME = "CheckingInductors.db";
    private static final int VERSION = 1;

    DatabaseHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SubjectTable.CREATE_SCRIPT);
        db.execSQL(SubjectTable.formInsertScript("Реле РКН-35", "5ТС.520.074"));
        db.execSQL(SubjectTable.formInsertScript("Реле РКЗ-306", "5ТН.520.242"));
        db.execSQL(SubjectTable.formInsertScript("Реле РКН-4", "5ТН.520.242,5ТС.520.074"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-1", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-82", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-9", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-84", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-86", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-85", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-96", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-32", "5ТН.520.237"));
        db.execSQL(SubjectTable.formInsertScript("Контактор МК-93", "5ТН.520.237"));

        db.execSQL(InductorTable.CREATE_SCRIPT);
        db.execSQL(InductorTable.formInsertScript("5ТС.520.074",
                "2700", "2430", "2970",
                "-1", "-1", "-1",
                "6500", "6000", "7000",
                "36", "34.2", "38.8",
                "9"));
        db.execSQL(InductorTable.formInsertScript("5ТН.520.242",
                "4800", "4320", "5280",
                "-1", "-1", "-1",
                "7000", "6650", "7350",
                "445", "422", "482",
                "10"));
        db.execSQL(InductorTable.formInsertScript("5ТН.520.237",
                "1500", "1350", "1650",
                "-1", "-1", "-1",
                "1500", "1425", "1575",
                "43.78", "41.58", "47.28", "11"));

        db.execSQL(ProtocolTable.CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SubjectTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + InductorTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ProtocolTable.TABLE_NAME);
        onCreate(db);
    }
}