package ru.avem.coilstestingfacility.database.model;

public class Inductor {
    private long mId;
    private String mName;
    private double mSpecifiedUTC;
    private double mSpecifiedUTCMinus;
    private double mSpecifiedUTCPlus;
    private double mSpecifiedITC;
    private double mSpecifiedITCMinus;
    private double mSpecifiedITCPlus;
    private double mSpecifiedUES;
    private double mSpecifiedUESMinus;
    private double mSpecifiedUESPlus;
    private double mSpecifiedR;
    private double mSpecifiedRMinus;
    private double mSpecifiedRPlus;
    private int mExperimentTime;

    public Inductor(long id, String name,
                    double specifiedUTC, double specifiedUTCMinus, double specifiedUTCPlus,
                    double specifiedITC, double specifiedITCMinus, double specifiedITCPlus,
                    double specifiedUES, double specifiedUESMinus, double specifiedUESPlus,
                    double specifiedR, double specifiedRMinus, double specifiedRPlus,
                    int experimentTime) {
        mId = id;
        mName = name;
        mSpecifiedUTC = specifiedUTC;
        mSpecifiedUTCMinus = specifiedUTCMinus;
        mSpecifiedUTCPlus = specifiedUTCPlus;
        mSpecifiedITC = specifiedITC;
        mSpecifiedITCMinus = specifiedITCMinus;
        mSpecifiedITCPlus = specifiedITCPlus;
        mSpecifiedUES = specifiedUES;
        mSpecifiedUESMinus = specifiedUESMinus;
        mSpecifiedUESPlus = specifiedUESPlus;
        mSpecifiedR = specifiedR;
        mSpecifiedRMinus = specifiedRMinus;
        mSpecifiedRPlus = specifiedRPlus;
        mExperimentTime = experimentTime;
    }

    public long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public double getSpecifiedUTC() {
        return mSpecifiedUTC;
    }

    public void setSpecifiedUTC(double specifiedUTC) {
        mSpecifiedUTC = specifiedUTC;
    }

    public double getSpecifiedUTCMinus() {
        return mSpecifiedUTCMinus;
    }

    public void setSpecifiedUTCMinus(double specifiedUTCMinus) {
        mSpecifiedUTCMinus = specifiedUTCMinus;
    }

    public double getSpecifiedUTCPlus() {
        return mSpecifiedUTCPlus;
    }

    public void setSpecifiedUTCPlus(double specifiedUTCPlus) {
        mSpecifiedUTCPlus = specifiedUTCPlus;
    }

    public double getSpecifiedITC() {
        return mSpecifiedITC;
    }

    public void setSpecifiedITC(double specifiedITC) {
        mSpecifiedITC = specifiedITC;
    }

    public double getSpecifiedITCMinus() {
        return mSpecifiedITCMinus;
    }

    public void setSpecifiedITCMinus(double specifiedITCMinus) {
        mSpecifiedITCMinus = specifiedITCMinus;
    }

    public double getSpecifiedITCPlus() {
        return mSpecifiedITCPlus;
    }

    public void setSpecifiedITCPlus(double specifiedITCPlus) {
        mSpecifiedITCPlus = specifiedITCPlus;
    }

    public double getSpecifiedUES() {
        return mSpecifiedUES;
    }

    public void setSpecifiedUES(double specifiedUES) {
        mSpecifiedUES = specifiedUES;
    }

    public double getSpecifiedUESMinus() {
        return mSpecifiedUESMinus;
    }

    public void setSpecifiedUESMinus(double specifiedUESMinus) {
        mSpecifiedUESMinus = specifiedUESMinus;
    }

    public double getSpecifiedUESPlus() {
        return mSpecifiedUESPlus;
    }

    public void setSpecifiedUESPlus(double specifiedUESPlus) {
        mSpecifiedUESPlus = specifiedUESPlus;
    }

    public double getSpecifiedR() {
        return mSpecifiedR;
    }

    public void setSpecifiedR(double specifiedR) {
        mSpecifiedR = specifiedR;
    }

    public double getSpecifiedRMinus() {
        return mSpecifiedRMinus;
    }

    public void setSpecifiedRMinus(double specifiedRMinus) {
        mSpecifiedRMinus = specifiedRMinus;
    }

    public double getSpecifiedRPlus() {
        return mSpecifiedRPlus;
    }

    public void setSpecifiedRPlus(double specifiedRPlus) {
        mSpecifiedRPlus = specifiedRPlus;
    }

    public int getExperimentTime() {
        return mExperimentTime;
    }

    public void setExperimentTime(int experimentTime) {
        mExperimentTime = experimentTime;
    }

    @Override
    public String toString() {
        return mName;
    }
}