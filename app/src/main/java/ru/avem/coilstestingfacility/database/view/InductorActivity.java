package ru.avem.coilstestingfacility.database.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.avem.coilstestingfacility.R;
import ru.avem.coilstestingfacility.database.model.DatabaseAdapter;
import ru.avem.coilstestingfacility.database.model.Inductor;
import ru.avem.coilstestingfacility.database.model.Subject;

public class InductorActivity extends AppCompatActivity {

    @BindView(R.id.name)
    EditText mName;
    @BindView(R.id.specified_U_TC)
    EditText mSpecifiedUTC;
    @BindView(R.id.specified_U_TC_Minus)
    EditText mSpecifiedUTCMinus;
    @BindView(R.id.specified_U_TC_Plus)
    EditText mSpecifiedUTCPlus;
    @BindView(R.id.specified_I_TC)
    EditText mSpecifiedITC;
    @BindView(R.id.specified_I_TC_Minus)
    EditText mSpecifiedITCMinus;
    @BindView(R.id.specified_I_TC_Plus)
    EditText mSpecifiedITCPlus;
    @BindView(R.id.specified_U_ES)
    EditText mSpecifiedUES;
    @BindView(R.id.specified_U_ES_Minus)
    EditText mSpecifiedUESMinus;
    @BindView(R.id.specified_U_ES_Plus)
    EditText mSpecifiedUESPlus;
    @BindView(R.id.specified_R)
    EditText mSpecifiedR;
    @BindView(R.id.specified_R_Minus)
    EditText mSpecifiedRMinus;
    @BindView(R.id.specified_R_Plus)
    EditText mSpecifiedRPlus;
    @BindView(R.id.experiment_time)
    EditText mExperimentTime;
    @BindView(R.id.saveButton)
    Button mSaveButton;
    @BindView(R.id.deleteButton)
    Button mDeleteButton;

    private DatabaseAdapter adapter;
    private long inductorId = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inductor);
        ButterKnife.bind(this);

        adapter = new DatabaseAdapter(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            inductorId = extras.getLong("id");
        }
        if (inductorId > 0) {
            adapter.open();
            Inductor inductor = adapter.getInductor(inductorId);
            mName.setText(inductor.getName());
            mName.setEnabled(false);
            mSpecifiedUTC.setText(String.format("%s", inductor.getSpecifiedUTC()));
            mSpecifiedUTCMinus.setText(String.format("%s", inductor.getSpecifiedUTCMinus()));
            mSpecifiedUTCPlus.setText(String.format("%s", inductor.getSpecifiedUTCPlus()));
            mSpecifiedITC.setText(String.format("%s", inductor.getSpecifiedITC()));
            mSpecifiedITCMinus.setText(String.format("%s", inductor.getSpecifiedITCMinus()));
            mSpecifiedITCPlus.setText(String.format("%s", inductor.getSpecifiedITCPlus()));
            mSpecifiedUES.setText(String.format("%s", inductor.getSpecifiedUES()));
            mSpecifiedUESMinus.setText(String.format("%s", inductor.getSpecifiedUESMinus()));
            mSpecifiedUESPlus.setText(String.format("%s", inductor.getSpecifiedUESPlus()));
            mSpecifiedR.setText(String.format("%s", inductor.getSpecifiedR()));
            mSpecifiedRMinus.setText(String.format("%s", inductor.getSpecifiedRMinus()));
            mSpecifiedRPlus.setText(String.format("%s", inductor.getSpecifiedRPlus()));
            mExperimentTime.setText(String.format("%s", inductor.getExperimentTime()));
            adapter.close();
        } else {
            mDeleteButton.setVisibility(View.GONE);
        }
    }

    public void save(View view) {
        if (mName.getText().length() > 0 && mSpecifiedUTC.getText().length() > 0 && mSpecifiedUTCMinus.getText().length() > 0 &&
                mSpecifiedUTCPlus.getText().length() > 0 && mSpecifiedUES.getText().length() > 0 && mSpecifiedUESMinus.getText().length() > 0 &&
                mSpecifiedUESPlus.getText().length() > 0 && mSpecifiedR.getText().length() > 0 && mSpecifiedRMinus.getText().length() > 0 &&
                mSpecifiedRPlus.getText().length() > 0 && mExperimentTime.getText().length() > 0) {
            String name = mName.getText().toString();
            double specifiedUTC = Double.parseDouble(mSpecifiedUTC.getText().toString());
            double specifiedUTCMinus = Double.parseDouble(mSpecifiedUTCMinus.getText().toString());
            double specifiedUTCPlus = Double.parseDouble(mSpecifiedUTCPlus.getText().toString());

            double specifiedITC;
            String specITC = mSpecifiedITC.getText().toString();
            if (TextUtils.isEmpty(specITC) || (Double.parseDouble(specITC) <= 0)) {
                specifiedITC = -1;
            } else {
                specifiedITC = Double.parseDouble(specITC);
            }

            double specifiedITCMinus;
            String specITCMinus = mSpecifiedITCMinus.getText().toString();
            if (TextUtils.isEmpty(specITCMinus) || (Double.parseDouble(specITCMinus) <= 0)) {
                specifiedITCMinus = -1;
            } else {
                specifiedITCMinus = Double.parseDouble(specITCMinus);
            }

            double specifiedITCPlus;
            String specITCPlus = mSpecifiedITCPlus.getText().toString();
            if (TextUtils.isEmpty(specITCPlus) || (Double.parseDouble(specITCPlus) <= 0)) {
                specifiedITCPlus = -1;
            } else {
                specifiedITCPlus = Double.parseDouble(specITCPlus);
            }

            double specifiedUES = Double.parseDouble(mSpecifiedUES.getText().toString());
            double specifiedUESMinus = Double.parseDouble(mSpecifiedUESMinus.getText().toString());
            double specifiedUESPlus = Double.parseDouble(mSpecifiedUESPlus.getText().toString());
            double specifiedR = Double.parseDouble(mSpecifiedR.getText().toString());
            double specifiedRMinus = Double.parseDouble(mSpecifiedRMinus.getText().toString());
            double specifiedRPlus = Double.parseDouble(mSpecifiedRPlus.getText().toString());
            int experimentTime = Integer.parseInt(mExperimentTime.getText().toString());
            Inductor inductor = new Inductor(inductorId, name, specifiedUTC, specifiedUTCMinus, specifiedUTCPlus, specifiedITC, specifiedITCMinus, specifiedITCPlus, specifiedUES, specifiedUESMinus,
                    specifiedUESPlus, specifiedR, specifiedRMinus, specifiedRPlus, experimentTime);

            adapter.open();
            if (inductorId > 0) {
                adapter.updateInductor(inductor);
            } else {
                adapter.insertInductor(inductor);
            }
            adapter.close();
            goHome();
        } else {
            Toast.makeText(this, "Заполните необходимые поля", Toast.LENGTH_SHORT).show();
        }
    }

    public void delete(View view) {
        adapter.open();
        List<Subject> subjects = adapter.getSubjects();
        for (Subject subject : subjects) {
            List<Inductor> inductors = subject.getInductors();
            for (Inductor inductor : inductors) {
                if (mName.getText().length() > 0 && (Objects.equals(inductor.getName(), mName.getText().toString()))) {
                    Toast.makeText(this, "Невозможно удалить, так как этот чертёж прикреплён к аппарату \"" + subject.getName() + "\"", Toast.LENGTH_LONG).show();
                    adapter.close();
                    return;
                }
            }
        }
        adapter.deleteInductor(inductorId);
        adapter.close();
        goHome();
    }

    private void goHome() {
        Intent intent = new Intent(this, DatabaseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}