package ru.avem.coilstestingfacility.database.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAdapter {
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDatabase;

    public DatabaseAdapter(Context context) {
        mDbHelper = new DatabaseHelper(context);
    }

    public DatabaseAdapter open() {
        mDatabase = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public List<Protocol> getProtocols() {
        ArrayList<Protocol> protocols = new ArrayList<>();
        Cursor cursor = getAllProtocols();
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.ID));
                String serialNumber = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SERIAL_NUMBER));
                String subjectName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SUBJECT_NAME));
                String inductorName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.INDUCTOR_NAME));
                double specifiedUTC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC));
                double specifiedUTCMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC_MINUS));
                double specifiedUTCPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC_PLUS));
                double objectUTC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_TC));
                double specifiedITC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC));
                double specifiedITCMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC_MINUS));
                double specifiedITCPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC_PLUS));
                double objectITC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_TC));
                int resultTC = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_TC));
                double specifiedUES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES));
                double specifiedUESMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES_MINUS));
                double specifiedUESPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES_PLUS));
                double objectUES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_ES));
                double objectIES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_ES));
                int resultES = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_ES));
                double specifiedR = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R));
                double specifiedRMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R_MINUS));
                double specifiedRPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R_PLUS));
                int experimentTime = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.EXPERIMENT_TIME));
                double objectR = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_R));
                int resultR = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_R));
                String position1 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1));
                String position1Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_NUMBER));
                String position1FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_FULL_NAME));
                String position2 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2));
                String position2Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_NUMBER));
                String position2FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_FULL_NAME));
                long date = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.DATE));
                protocols.add(new Protocol(id, serialNumber, subjectName, inductorName,
                        specifiedUTC, specifiedUTCMinus, specifiedUTCPlus,
                        objectUTC,
                        specifiedITC, specifiedITCMinus, specifiedITCPlus,
                        objectITC,
                        resultTC == 1,
                        specifiedUES, specifiedUESMinus, specifiedUESPlus,
                        objectUES,
                        objectIES,
                        resultES == 1,
                        specifiedR, specifiedRMinus, specifiedRPlus,
                        experimentTime,
                        objectR,
                        resultR == 1,
                        position1, position1Number, position1FullName,
                        position2, position2Number, position2FullName, date));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return protocols;
    }

    private Cursor getAllProtocols() {
        String[] columns = new String[]{
                ProtocolTable.COLUMN.ID,
                ProtocolTable.COLUMN.SERIAL_NUMBER,
                ProtocolTable.COLUMN.SUBJECT_NAME,
                ProtocolTable.COLUMN.INDUCTOR_NAME,
                ProtocolTable.COLUMN.SPECIFIED_U_TC,
                ProtocolTable.COLUMN.SPECIFIED_U_TC_MINUS,
                ProtocolTable.COLUMN.SPECIFIED_U_TC_PLUS,
                ProtocolTable.COLUMN.OBJECT_U_TC,
                ProtocolTable.COLUMN.SPECIFIED_I_TC,
                ProtocolTable.COLUMN.SPECIFIED_I_TC_MINUS,
                ProtocolTable.COLUMN.SPECIFIED_I_TC_PLUS,
                ProtocolTable.COLUMN.OBJECT_I_TC,
                ProtocolTable.COLUMN.RESULT_TC,
                ProtocolTable.COLUMN.SPECIFIED_U_ES,
                ProtocolTable.COLUMN.SPECIFIED_U_ES_MINUS,
                ProtocolTable.COLUMN.SPECIFIED_U_ES_PLUS,
                ProtocolTable.COLUMN.OBJECT_U_ES,
                ProtocolTable.COLUMN.OBJECT_I_ES,
                ProtocolTable.COLUMN.RESULT_ES,
                ProtocolTable.COLUMN.SPECIFIED_R,
                ProtocolTable.COLUMN.SPECIFIED_R_MINUS,
                ProtocolTable.COLUMN.SPECIFIED_R_PLUS,
                ProtocolTable.COLUMN.EXPERIMENT_TIME,
                ProtocolTable.COLUMN.OBJECT_R,
                ProtocolTable.COLUMN.RESULT_R,
                ProtocolTable.COLUMN.POSITION1,
                ProtocolTable.COLUMN.POSITION1_NUMBER,
                ProtocolTable.COLUMN.POSITION1_FULL_NAME,
                ProtocolTable.COLUMN.POSITION2,
                ProtocolTable.COLUMN.POSITION2_NUMBER,
                ProtocolTable.COLUMN.POSITION2_FULL_NAME,
                ProtocolTable.COLUMN.DATE};
        return mDatabase.query(ProtocolTable.TABLE_NAME, columns, null, null, null, null, null);
    }

    public Protocol getProtocol(long id) {
        Protocol protocol = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", ProtocolTable.TABLE_NAME, ProtocolTable.COLUMN.ID);
        Cursor cursor = mDatabase.rawQuery(query, new String[]{String.valueOf(id)});
        if (cursor.moveToFirst()) {
            String serialNumber = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SERIAL_NUMBER));
            String subjectName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SUBJECT_NAME));
            String inductorName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.INDUCTOR_NAME));
            double specifiedUTC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC));
            double specifiedUTCMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC_MINUS));
            double specifiedUTCPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC_PLUS));
            double objectUTC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_TC));
            double specifiedITC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC));
            double specifiedITCMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC_MINUS));
            double specifiedITCPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC_PLUS));
            double objectITC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_TC));
            int resultTC = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_TC));
            double specifiedUES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES));
            double specifiedUESMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES_MINUS));
            double specifiedUESPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES_PLUS));
            double objectUES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_ES));
            double objectIES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_ES));
            int resultES = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_ES));
            double specifiedR = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R));
            double specifiedRMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R_MINUS));
            double specifiedRPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R_PLUS));
            int experimentTime = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.EXPERIMENT_TIME));
            double objectR = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_R));
            int resultR = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_R));
            String position1 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1));
            String position1Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_NUMBER));
            String position1FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_FULL_NAME));
            String position2 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2));
            String position2Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_NUMBER));
            String position2FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_FULL_NAME));
            long date = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.DATE));
            protocol = (new Protocol(id, serialNumber, subjectName, inductorName,
                    specifiedUTC, specifiedUTCMinus, specifiedUTCPlus,
                    objectUTC,
                    specifiedITC, specifiedITCMinus, specifiedITCPlus,
                    objectITC,
                    resultTC == 1,
                    specifiedUES, specifiedUESMinus, specifiedUESPlus,
                    objectUES,
                    objectIES,
                    resultES == 1,
                    specifiedR, specifiedRMinus, specifiedRPlus,
                    experimentTime,
                    objectR,
                    resultR == 1,
                    position1, position1Number, position1FullName,
                    position2, position2Number, position2FullName, date));
        }
        cursor.close();
        return protocol;
    }

    public Protocol getProtocol(String protocolName) {
        Protocol protocol = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", ProtocolTable.TABLE_NAME, ProtocolTable.COLUMN.SERIAL_NUMBER);
        Cursor cursor = mDatabase.rawQuery(query, new String[]{String.valueOf(protocolName)});
        if (cursor.moveToFirst()) {
            long id = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.ID));
            String serialNumber = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SERIAL_NUMBER));
            String subjectName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.SUBJECT_NAME));
            String inductorName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.INDUCTOR_NAME));
            double specifiedUTC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC));
            double specifiedUTCMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC_MINUS));
            double specifiedUTCPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_TC_PLUS));
            double objectUTC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_TC));
            double specifiedITC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC));
            double specifiedITCMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC_MINUS));
            double specifiedITCPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_I_TC_PLUS));
            double objectITC = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_TC));
            int resultTC = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_TC));
            double specifiedUES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES));
            double specifiedUESMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES_MINUS));
            double specifiedUESPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_U_ES_PLUS));
            double objectUES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_U_ES));
            double objectIES = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_I_ES));
            int resultES = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_ES));
            double specifiedR = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R));
            double specifiedRMinus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R_MINUS));
            double specifiedRPlus = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.SPECIFIED_R_PLUS));
            int experimentTime = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.EXPERIMENT_TIME));
            double objectR = cursor.getDouble(cursor.getColumnIndex(ProtocolTable.COLUMN.OBJECT_R));
            int resultR = cursor.getInt(cursor.getColumnIndex(ProtocolTable.COLUMN.RESULT_R));
            String position1 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1));
            String position1Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_NUMBER));
            String position1FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION1_FULL_NAME));
            String position2 = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2));
            String position2Number = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_NUMBER));
            String position2FullName = cursor.getString(cursor.getColumnIndex(ProtocolTable.COLUMN.POSITION2_FULL_NAME));
            long date = cursor.getLong(cursor.getColumnIndex(ProtocolTable.COLUMN.DATE));
            protocol = (new Protocol(id, serialNumber, subjectName, inductorName,
                    specifiedUTC, specifiedUTCMinus, specifiedUTCPlus,
                    objectUTC,
                    specifiedITC, specifiedITCMinus, specifiedITCPlus,
                    objectITC,
                    resultTC == 1,
                    specifiedUES, specifiedUESMinus, specifiedUESPlus,
                    objectUES,
                    objectIES,
                    resultES == 1,
                    specifiedR, specifiedRMinus, specifiedRPlus,
                    experimentTime,
                    objectR,
                    resultR == 1,
                    position1, position1Number, position1FullName,
                    position2, position2Number, position2FullName, date));
        }
        cursor.close();
        return protocol;
    }

    public long insertProtocol(Protocol protocol) {
        ContentValues cv = new ContentValues();
        cv.put(ProtocolTable.COLUMN.SERIAL_NUMBER, protocol.getSerialNumber());
        cv.put(ProtocolTable.COLUMN.SUBJECT_NAME, protocol.getSubjectName());
        cv.put(ProtocolTable.COLUMN.INDUCTOR_NAME, protocol.getInductorName());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_TC, protocol.getSpecifiedUTC());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_TC_MINUS, protocol.getSpecifiedUTCMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_TC_PLUS, protocol.getSpecifiedUTCPlus());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_TC, protocol.getObjectUTC());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I_TC, protocol.getSpecifiedITC());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I_TC_MINUS, protocol.getSpecifiedITCMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I_TC_PLUS, protocol.getSpecifiedITCPlus());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_TC, protocol.getObjectITC());
        cv.put(ProtocolTable.COLUMN.RESULT_TC, protocol.isResultTC() ? 1 : 0);
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_ES, protocol.getSpecifiedUES());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_ES_MINUS, protocol.getSpecifiedUESMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_ES_PLUS, protocol.getSpecifiedUESPlus());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_ES, protocol.getObjectUES());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_ES, protocol.getObjectIES());
        cv.put(ProtocolTable.COLUMN.RESULT_ES, protocol.isResultES() ? 1 : 0);
        cv.put(ProtocolTable.COLUMN.SPECIFIED_R, protocol.getSpecifiedR());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_R_MINUS, protocol.getSpecifiedRMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_R_PLUS, protocol.getSpecifiedRPlus());
        cv.put(ProtocolTable.COLUMN.EXPERIMENT_TIME, protocol.getExperimentTime());
        cv.put(ProtocolTable.COLUMN.OBJECT_R, protocol.getObjectR());
        cv.put(ProtocolTable.COLUMN.RESULT_R, protocol.isResultR() ? 1 : 0);
        cv.put(ProtocolTable.COLUMN.POSITION1, protocol.getPosition1());
        cv.put(ProtocolTable.COLUMN.POSITION1_NUMBER, protocol.getPosition1Number());
        cv.put(ProtocolTable.COLUMN.POSITION1_FULL_NAME, protocol.getPosition1FullName());
        cv.put(ProtocolTable.COLUMN.POSITION2, protocol.getPosition2());
        cv.put(ProtocolTable.COLUMN.POSITION2_NUMBER, protocol.getPosition2Number());
        cv.put(ProtocolTable.COLUMN.POSITION2_FULL_NAME, protocol.getPosition2FullName());
        cv.put(ProtocolTable.COLUMN.DATE, protocol.getDate());
        return mDatabase.insert(ProtocolTable.TABLE_NAME, null, cv);
    }

    public long deleteProtocol(long protocolId) {
        String whereClause = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(protocolId)};
        return mDatabase.delete(ProtocolTable.TABLE_NAME, whereClause, whereArgs);
    }

    public long updateProtocol(Protocol protocol) {
        String whereClause = ProtocolTable.COLUMN.ID + "=" + String.valueOf(protocol.getId());
        ContentValues cv = new ContentValues();
        cv.put(ProtocolTable.COLUMN.SERIAL_NUMBER, protocol.getSerialNumber());
        cv.put(ProtocolTable.COLUMN.SUBJECT_NAME, protocol.getSubjectName());
        cv.put(ProtocolTable.COLUMN.INDUCTOR_NAME, protocol.getInductorName());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_TC, protocol.getSpecifiedUTC());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_TC_MINUS, protocol.getSpecifiedUTCMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_TC_PLUS, protocol.getSpecifiedUTCPlus());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_TC, protocol.getObjectUTC());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I_TC, protocol.getSpecifiedITC());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I_TC_MINUS, protocol.getSpecifiedITCMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_I_TC_PLUS, protocol.getSpecifiedITCPlus());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_TC, protocol.getObjectITC());
        cv.put(ProtocolTable.COLUMN.RESULT_TC, protocol.isResultTC() ? 1 : 0);
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_ES, protocol.getSpecifiedUES());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_ES_MINUS, protocol.getSpecifiedUESMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_U_ES_PLUS, protocol.getSpecifiedUESPlus());
        cv.put(ProtocolTable.COLUMN.OBJECT_U_ES, protocol.getObjectUES());
        cv.put(ProtocolTable.COLUMN.OBJECT_I_ES, protocol.getObjectIES());
        cv.put(ProtocolTable.COLUMN.RESULT_ES, protocol.isResultES() ? 1 : 0);
        cv.put(ProtocolTable.COLUMN.SPECIFIED_R, protocol.getSpecifiedR());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_R_MINUS, protocol.getSpecifiedRMinus());
        cv.put(ProtocolTable.COLUMN.SPECIFIED_R_PLUS, protocol.getSpecifiedRPlus());
        cv.put(ProtocolTable.COLUMN.EXPERIMENT_TIME, protocol.getExperimentTime());
        cv.put(ProtocolTable.COLUMN.OBJECT_R, protocol.getObjectR());
        cv.put(ProtocolTable.COLUMN.RESULT_R, protocol.isResultR() ? 1 : 0);
        cv.put(ProtocolTable.COLUMN.POSITION1, protocol.getPosition1());
        cv.put(ProtocolTable.COLUMN.POSITION1_NUMBER, protocol.getPosition1Number());
        cv.put(ProtocolTable.COLUMN.POSITION1_FULL_NAME, protocol.getPosition1FullName());
        cv.put(ProtocolTable.COLUMN.POSITION2, protocol.getPosition2());
        cv.put(ProtocolTable.COLUMN.POSITION2_NUMBER, protocol.getPosition2Number());
        cv.put(ProtocolTable.COLUMN.POSITION2_FULL_NAME, protocol.getPosition2FullName());
        cv.put(ProtocolTable.COLUMN.DATE, protocol.getDate());
        return mDatabase.update(ProtocolTable.TABLE_NAME, cv, whereClause, null);
    }

    public List<Subject> getSubjects() {
        ArrayList<Subject> subjects = new ArrayList<>();
        Cursor cursor = getAllSubjects();
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(SubjectTable.COLUMN.ID));
                String name = cursor.getString(cursor.getColumnIndex(SubjectTable.COLUMN.NAME));
                String inductors = cursor.getString(cursor.getColumnIndex(SubjectTable.COLUMN.INDUCTORS));
                subjects.add(new Subject(id, name, Subject.stringToList(inductors, this)));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return subjects;
    }

    private Cursor getAllSubjects() {
        String[] columns = new String[]{SubjectTable.COLUMN.ID, SubjectTable.COLUMN.NAME,
                SubjectTable.COLUMN.INDUCTORS};
        return mDatabase.query(SubjectTable.TABLE_NAME, columns, null, null, null, null, null);
    }

    public Subject getSubject(long id) {
        Subject subject = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", SubjectTable.TABLE_NAME, SubjectTable.COLUMN.ID);
        Cursor cursor = mDatabase.rawQuery(query, new String[]{String.valueOf(id)});
        if (cursor.moveToFirst()) {
            String name = cursor.getString(cursor.getColumnIndex(InductorTable.COLUMN.NAME));
            String inductors = cursor.getString(cursor.getColumnIndex(SubjectTable.COLUMN.INDUCTORS));
            subject = new Subject(id, name, Subject.stringToList(inductors, this));
        }
        cursor.close();
        return subject;
    }

    public long insertSubject(Subject subject) {
        ContentValues cv = new ContentValues();
        cv.put(SubjectTable.COLUMN.NAME, subject.getName());
        cv.put(SubjectTable.COLUMN.INDUCTORS, Subject.listToString(subject.getInductors()));
        return mDatabase.insert(SubjectTable.TABLE_NAME, null, cv);
    }

    public long deleteSubject(long subjectId) {
        String whereClause = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(subjectId)};
        return mDatabase.delete(SubjectTable.TABLE_NAME, whereClause, whereArgs);
    }

    public long updateSubject(Subject subject) {
        String whereClause = SubjectTable.COLUMN.ID + "=" + String.valueOf(subject.getId());
        ContentValues cv = new ContentValues();
        cv.put(SubjectTable.COLUMN.NAME, subject.getName());
        cv.put(SubjectTable.COLUMN.INDUCTORS, Subject.listToString(subject.getInductors()));
        return mDatabase.update(SubjectTable.TABLE_NAME, cv, whereClause, null);
    }

    public List<Inductor> getInductors() {
        ArrayList<Inductor> inductors = new ArrayList<>();
        Cursor cursor = getAllInductors();
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(InductorTable.COLUMN.ID));
                String name = cursor.getString(cursor.getColumnIndex(InductorTable.COLUMN.NAME));
                double specifiedUTC = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC));
                double specifiedUTCMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC_Minus));
                double specifiedUTCPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC_Plus));
                double specifiedITC = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC));
                double specifiedITCMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC_Minus));
                double specifiedITCPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC_Plus));
                double specifiedUES = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES));
                double specifiedUESMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES_Minus));
                double specifiedUESPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES_Plus));
                double specifiedR = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R));
                double specifiedRMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R_Minus));
                double specifiedRPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R_Plus));
                int experimentTime = cursor.getInt(cursor.getColumnIndex(InductorTable.COLUMN.EXPERIMENT_TIME));
                inductors.add(new Inductor(
                        id,
                        name,
                        specifiedUTC,
                        specifiedUTCMinus,
                        specifiedUTCPlus,
                        specifiedITC,
                        specifiedITCMinus,
                        specifiedITCPlus,
                        specifiedUES,
                        specifiedUESMinus,
                        specifiedUESPlus,
                        specifiedR,
                        specifiedRMinus,
                        specifiedRPlus,
                        experimentTime));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return inductors;
    }

    private Cursor getAllInductors() {
        String[] columns = new String[]{
                InductorTable.COLUMN.ID,
                InductorTable.COLUMN.NAME,
                InductorTable.COLUMN.SPECIFIED_U_TC,
                InductorTable.COLUMN.SPECIFIED_U_TC_Minus,
                InductorTable.COLUMN.SPECIFIED_U_TC_Plus,
                InductorTable.COLUMN.SPECIFIED_I_TC,
                InductorTable.COLUMN.SPECIFIED_I_TC_Minus,
                InductorTable.COLUMN.SPECIFIED_I_TC_Plus,
                InductorTable.COLUMN.SPECIFIED_U_ES,
                InductorTable.COLUMN.SPECIFIED_U_ES_Minus,
                InductorTable.COLUMN.SPECIFIED_U_ES_Plus,
                InductorTable.COLUMN.SPECIFIED_R,
                InductorTable.COLUMN.SPECIFIED_R_Minus,
                InductorTable.COLUMN.SPECIFIED_R_Plus,
                InductorTable.COLUMN.EXPERIMENT_TIME};
        return mDatabase.query(InductorTable.TABLE_NAME, columns, null, null, null, null, null);
    }

    public long getCountOfInductors() {
        return DatabaseUtils.queryNumEntries(mDatabase, InductorTable.TABLE_NAME);
    }

    public Inductor getInductor(long id) {
        Inductor inductor = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", InductorTable.TABLE_NAME, InductorTable.COLUMN.ID);
        Cursor cursor = mDatabase.rawQuery(query, new String[]{String.valueOf(id)});
        if (cursor.moveToFirst()) {
            String name = cursor.getString(cursor.getColumnIndex(InductorTable.COLUMN.NAME));
            double specifiedUTC = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC));
            double specifiedUTCMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC_Minus));
            double specifiedUTCPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC_Plus));
            double specifiedITC = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC));
            double specifiedITCMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC_Minus));
            double specifiedITCPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC_Plus));
            double specifiedUES = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES));
            double specifiedUESMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES_Minus));
            double specifiedUESPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES_Plus));
            double specifiedR = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R));
            double specifiedRMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R_Minus));
            double specifiedRPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R_Plus));
            int experimentTime = cursor.getInt(cursor.getColumnIndex(InductorTable.COLUMN.EXPERIMENT_TIME));
            inductor = new Inductor(
                    id,
                    name,
                    specifiedUTC,
                    specifiedUTCMinus,
                    specifiedUTCPlus,
                    specifiedITC,
                    specifiedITCMinus,
                    specifiedITCPlus,
                    specifiedUES,
                    specifiedUESMinus,
                    specifiedUESPlus,
                    specifiedR,
                    specifiedRMinus,
                    specifiedRPlus,
                    experimentTime);
        }
        cursor.close();
        return inductor;
    }

    public Inductor getInductor(String inductorName) {
        Inductor inductor = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", InductorTable.TABLE_NAME, InductorTable.COLUMN.NAME);
        Cursor cursor = mDatabase.rawQuery(query, new String[]{String.valueOf(inductorName)});
        if (cursor.moveToFirst()) {
            long id = cursor.getLong(cursor.getColumnIndex(InductorTable.COLUMN.ID));
            String name = cursor.getString(cursor.getColumnIndex(InductorTable.COLUMN.NAME));
            double specifiedUTC = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC));
            double specifiedUTCMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC_Minus));
            double specifiedUTCPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_TC_Plus));
            double specifiedITC = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC));
            double specifiedITCMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC_Minus));
            double specifiedITCPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_I_TC_Plus));
            double specifiedUES = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES));
            double specifiedUESMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES_Minus));
            double specifiedUESPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_U_ES_Plus));
            double specifiedR = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R));
            double specifiedRMinus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R_Minus));
            double specifiedRPlus = cursor.getDouble(cursor.getColumnIndex(InductorTable.COLUMN.SPECIFIED_R_Plus));
            int experimentTime = cursor.getInt(cursor.getColumnIndex(InductorTable.COLUMN.EXPERIMENT_TIME));
            inductor = new Inductor(
                    id,
                    name,
                    specifiedUTC,
                    specifiedUTCMinus,
                    specifiedUTCPlus,
                    specifiedITC,
                    specifiedITCMinus,
                    specifiedITCPlus,
                    specifiedUES,
                    specifiedUESMinus,
                    specifiedUESPlus,
                    specifiedR,
                    specifiedRMinus,
                    specifiedRPlus,
                    experimentTime);
        }
        cursor.close();
        return inductor;
    }

    public long insertInductor(Inductor inductor) {
        ContentValues cv = new ContentValues();
        cv.put(InductorTable.COLUMN.NAME, inductor.getName());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_TC, inductor.getSpecifiedUTC());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_TC_Minus, inductor.getSpecifiedUTCMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_TC_Plus, inductor.getSpecifiedUTCPlus());
        cv.put(InductorTable.COLUMN.SPECIFIED_I_TC, inductor.getSpecifiedITC());
        cv.put(InductorTable.COLUMN.SPECIFIED_I_TC_Minus, inductor.getSpecifiedITCMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_I_TC_Plus, inductor.getSpecifiedITCPlus());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_ES, inductor.getSpecifiedUES());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_ES_Minus, inductor.getSpecifiedUESMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_ES_Plus, inductor.getSpecifiedUESPlus());
        cv.put(InductorTable.COLUMN.SPECIFIED_R, inductor.getSpecifiedR());
        cv.put(InductorTable.COLUMN.SPECIFIED_R_Minus, inductor.getSpecifiedRMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_R_Plus, inductor.getSpecifiedRPlus());
        cv.put(InductorTable.COLUMN.EXPERIMENT_TIME, inductor.getExperimentTime());
        return mDatabase.insert(InductorTable.TABLE_NAME, null, cv);
    }

    public long deleteInductor(long inductorId) {
        String whereClause = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(inductorId)};
        return mDatabase.delete(InductorTable.TABLE_NAME, whereClause, whereArgs);
    }

    public long updateInductor(Inductor inductor) {
        String whereClause = InductorTable.COLUMN.ID + "=" + String.valueOf(inductor.getId());
        ContentValues cv = new ContentValues();
        cv.put(InductorTable.COLUMN.NAME, inductor.getName());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_TC, inductor.getSpecifiedUTC());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_TC_Minus, inductor.getSpecifiedUTCMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_TC_Plus, inductor.getSpecifiedUTCPlus());
        cv.put(InductorTable.COLUMN.SPECIFIED_I_TC, inductor.getSpecifiedITC());
        cv.put(InductorTable.COLUMN.SPECIFIED_I_TC_Minus, inductor.getSpecifiedITCMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_I_TC_Plus, inductor.getSpecifiedITCPlus());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_ES, inductor.getSpecifiedUES());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_ES_Minus, inductor.getSpecifiedUESMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_U_ES_Plus, inductor.getSpecifiedUESPlus());
        cv.put(InductorTable.COLUMN.SPECIFIED_R, inductor.getSpecifiedR());
        cv.put(InductorTable.COLUMN.SPECIFIED_R_Minus, inductor.getSpecifiedRMinus());
        cv.put(InductorTable.COLUMN.SPECIFIED_R_Plus, inductor.getSpecifiedRPlus());
        cv.put(InductorTable.COLUMN.EXPERIMENT_TIME, inductor.getExperimentTime());
        return mDatabase.update(InductorTable.TABLE_NAME, cv, whereClause, null);
    }
}