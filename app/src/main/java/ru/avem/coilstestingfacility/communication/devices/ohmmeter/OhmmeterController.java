package ru.avem.coilstestingfacility.communication.devices.ohmmeter;


import android.util.Log;

import java.nio.ByteBuffer;
import java.util.Observer;

import ru.avem.coilstestingfacility.communication.devices.DeviceController;
import ru.avem.coilstestingfacility.communication.protocol.ohmmeter.OhmmeterProtocol;

public class OhmmeterController implements DeviceController {
    private OhmmeterModel mModel;
    private OhmmeterProtocol mOhmmeterProtocol;
    private byte mAttempt = NUMBER_OF_ATTEMPTS;
    private boolean mNeedToReed;

    public OhmmeterController(Observer observer, OhmmeterProtocol protocol) {
        mModel = new OhmmeterModel(observer);
        mOhmmeterProtocol = protocol;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        int attempt = 10;
        boolean status;
        do {
            status = mOhmmeterProtocol.readData(inputBuffer);
        } while (!status && --attempt > 0);
        if (status) {
            Log.i("DEBUG_TAG", "read: true");
            mModel.setResponding(true);
            mModel.setMMode(inputBuffer.get());
            mModel.setMResistance(inputBuffer.getShort());
            mModel.setMScope(inputBuffer.get());
            mModel.setMStatus(inputBuffer.get());
        } else {
            mModel.setResponding(false);
            Log.i("DEBUG_TAG", "read: false");
        }
    }

    @Override
    public void write(Object... args) {

    }

    @Override
    public void resetAttempts() {
        mAttempt = NUMBER_OF_ATTEMPTS;
    }

    @Override
    public boolean thereAreAttempts() {
        return mAttempt > 0;
    }

    @Override
    public boolean needToRead() {
        return mNeedToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        mNeedToReed = needToRead;
    }
}
