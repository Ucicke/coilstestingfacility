package ru.avem.coilstestingfacility.communication.devices;

import android.content.Context;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import ru.avem.coilstestingfacility.communication.devices.ammeter.AmmeterController;
import ru.avem.coilstestingfacility.communication.devices.ohmmeter.OhmmeterController;
import ru.avem.coilstestingfacility.communication.devices.pm130.PM130Controller;
import ru.avem.coilstestingfacility.communication.devices.pr.PRController;
import ru.avem.coilstestingfacility.communication.devices.sheem.SheemController;
import ru.avem.coilstestingfacility.communication.devices.voltmeter.VoltmeterController;
import ru.avem.coilstestingfacility.communication.protocol.modbus.ModbusController;
import ru.avem.coilstestingfacility.communication.protocol.modbus.RTUController;
import ru.avem.coilstestingfacility.communication.protocol.ohmmeter.OhmmeterProtocol;
import ru.avem.coilstestingfacility.communication.serial.SerialConnection;

import static ru.avem.coilstestingfacility.communication.devices.pr.PRController.DO_UP_REGISTER;
import static ru.avem.coilstestingfacility.communication.devices.pr.PRController.RESET_DOG_REGISTER;
import static ru.avem.coilstestingfacility.communication.devices.pr.PRController.RESET_TIME_REGISTER;
import static ru.avem.coilstestingfacility.communication.devices.pr.PRController.SWITCH_TRANSF_KM_REGISTER;
import static ru.avem.coilstestingfacility.communication.devices.sheem.SheemController.FILLING_LEVEL_REGISTER;
import static ru.avem.coilstestingfacility.communication.devices.sheem.SheemController.SWITCH_REGISTER;

public class DevicesController implements Runnable {
    private static final int WRITE_TIMEOUT = 25;
    private static final int READ_TIMEOUT = 25;

    private static final String RS485_DEVICE_NAME = "CP2103 USB to RS-485";
    private static final String OHMMETER_DEVICE_NAME = "CP2102 USB to UART Bridge Controller";
    private static final int BAUD_RATE = 38400;

    private SerialConnection mRS485Connection;
    private SerialConnection mOhmmeterConnection;

    private List<DeviceController> mDevicesControllers = new ArrayList<>();

    private DeviceController mPM130Controller;
    private DeviceController mPRController;

    private DeviceController mSheemController;
    private DeviceController mAmmeterController;
    private DeviceController mVoltmeterController;

    private DeviceController mOhmmeterController;

    private boolean mLastOne;
    private long mLastTime;

    public DevicesController(Context context, Observer observer) {
        mRS485Connection = new SerialConnection(context, RS485_DEVICE_NAME, BAUD_RATE,
                UsbSerialPort.DATABITS_8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE,
                WRITE_TIMEOUT, READ_TIMEOUT);
        ModbusController modbusController = new RTUController(mRS485Connection);


        mSheemController = new SheemController(observer, modbusController);
        mDevicesControllers.add(mSheemController);
        mAmmeterController = new AmmeterController(observer, modbusController);
        mDevicesControllers.add(mAmmeterController);
        mVoltmeterController = new VoltmeterController(observer, modbusController);
        mDevicesControllers.add(mVoltmeterController);

        mPM130Controller = new PM130Controller(observer, modbusController);
        mDevicesControllers.add(mPM130Controller);
        mPRController = new PRController(observer, modbusController);
        mDevicesControllers.add(mPRController);

        OhmmeterProtocol ohmModbusProtocol = new OhmmeterProtocol(context, OHMMETER_DEVICE_NAME);
        mOhmmeterConnection = ohmModbusProtocol.getConnection();
        mOhmmeterController = new OhmmeterController(observer, ohmModbusProtocol);
//        mDevicesControllers.add(mOhmmeterController);

        Thread continuousReadingThread = new Thread(this);
        continuousReadingThread.start();
        mLastTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime > (mLastTime + 60000)) {
                mLastTime = currentTime;
                mOhmmeterController.read();
            }
            for (DeviceController deviceController : mDevicesControllers) {
                if (deviceController.needToRead() && deviceController.thereAreAttempts()) {
                    deviceController.read();
                    if (deviceController instanceof PRController) {
                        resetDog();
                    }
                }
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void resetDog() {
        if (mLastOne) {
//            mPRController.write(RESET_DOG_REGISTER, (short) 0);
            mPRController.write(RESET_DOG_REGISTER, 1, 0);
            mLastOne = false;
        } else {
//            mPRController.write(RESET_DOG_REGISTER, (short) 1);
            mPRController.write(RESET_DOG_REGISTER, 1, 1);
            mLastOne = true;
        }
    }

    public void resetTimer() {
//        mPRController.write(RESET_DOG_REGISTER, (short) 0);
//        mPRController.write(RESET_DOG_REGISTER, (short) 1);
        mPRController.write(RESET_DOG_REGISTER, 1, 0);
        mPRController.write(RESET_DOG_REGISTER, 1, 1);
        mLastOne = true;
//        mPRController.write(RESET_TIME_REGISTER, (short) 1);
//        mPRController.write(RESET_TIME_REGISTER, (short) 0);
        mPRController.write(RESET_TIME_REGISTER, 1, 1);
        mPRController.write(RESET_TIME_REGISTER, 1, 0);
    }

    public void connectSheem() {
        checkRS485Connection();
        mSheemController.resetAttempts();
    }

    private void checkRS485Connection() {
        if (!mRS485Connection.isInitiated()) {
            try {
                mRS485Connection.initSerialPort();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkOhmmeterConnection() {
        if (!mOhmmeterConnection.isInitiated()) {
            try {
                mOhmmeterConnection.initSerialPort();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void connectVoltmeter() {
        checkRS485Connection();
        mVoltmeterController.resetAttempts();
    }

    public void connectAmmeter() {
        checkRS485Connection();
        mAmmeterController.resetAttempts();
    }

    public void connectPR() {
        checkRS485Connection();
        mPRController.resetAttempts();
    }

    public void connectPM130() {
        checkRS485Connection();
        mPM130Controller.resetAttempts();
    }

    public void connectOhmmeter() {
        checkOhmmeterConnection();
        mOhmmeterController.read();
    }

    public void readOhmmeter() {
        mOhmmeterController.resetAttempts();
        for (int i = 0; i < 10; i++) {
            mOhmmeterController.read();
        }
    }

    public void startWithOnTransfKmLATR() {
//        mPRController.write(SWITCH_TRANSF_KM_REGISTER, (short) 1);
        mPRController.write(SWITCH_TRANSF_KM_REGISTER, 3, 1, 1, 0);
    }

    public void offTransfKmLATR() {
//        mPRController.write(SWITCH_TRANSF_KM_REGISTER, (short) 0);
        mPRController.write(SWITCH_TRANSF_KM_REGISTER, 1, 0);
    }
    public void stopWithOffTransfKmLATR() {
        mPRController.write(SWITCH_TRANSF_KM_REGISTER, 3, 0, 0, 0);
    }

    public void startUpLATR() {
//        mPRController.write(DO_DOWN_REGISTER, (short) 0);
//        mPRController.write(DO_UP_REGISTER, (short) 1);
        mPRController.write(DO_UP_REGISTER, 2, 1, 0);
    }

    public void startDownLATR() {
//        mPRController.write(DO_UP_REGISTER, (short) 0);
//        mPRController.write(DO_DOWN_REGISTER, (short) 1);
        mPRController.write(DO_UP_REGISTER, 2, 0, 1);
    }

    public void stopLATR() {
//        mPRController.write(DO_UP_REGISTER, (short) 0);
//        mPRController.write(DO_DOWN_REGISTER, (short) 0);
        mPRController.write(DO_UP_REGISTER, 2, 0, 0);
    }

    public void setFillingLevelSheem(short fillingLevel) {
        mSheemController.write(FILLING_LEVEL_REGISTER, fillingLevel);
    }

    public void onKMSheem() {
        mSheemController.write(SWITCH_REGISTER, (short) 1);
    }

    public void offKMSheem() {
        mSheemController.write(SWITCH_REGISTER, (short) 0);
    }

    public void connectMainBus() {
        if (!mRS485Connection.isInitiated()) {
            try {
                mRS485Connection.initSerialPort();
                connectSheem();
                connectVoltmeter();
                connectAmmeter();
                connectPR();
                connectPM130();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void connectOhmmeterBus() {
        if (!mOhmmeterConnection.isInitiated()) {
            Log.i("DEBUG_TAG", "connectOhmmeterBus: ");
            try {
                mOhmmeterConnection.initSerialPort();
                connectOhmmeter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void disconnectMainBus() {
        mRS485Connection.closeSerialPort();
    }

    public void disconnectOhmmeterBus() {
        mOhmmeterConnection.closeSerialPort();
    }

    public void initTCDevices() {
        connectMainBus();
        mSheemController.setNeedToRead(true);
        mAmmeterController.setNeedToRead(true);
        mVoltmeterController.setNeedToRead(true);
    }

    public void initESDevices() {
        connectMainBus();
        mPM130Controller.setNeedToRead(true);
        mPRController.setNeedToRead(true);
    }

    public void initOHMDevices() {
        connectOhmmeterBus();
    }

    public void diversifyDevices() {
        for (DeviceController devicesController : mDevicesControllers) {
            devicesController.setNeedToRead(false);
        }
        disconnectMainBus();
//        disconnectOhmmeterBus();
    }
}