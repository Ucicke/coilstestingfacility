package ru.avem.coilstestingfacility.communication.devices;

public interface DeviceController {
    int INPUT_BUFFER_SIZE = 256;
    int NUMBER_OF_ATTEMPTS = 3;

    int SHEEM_ID = 0;
    int VOLTMETER_ID = 1;
    int AMMETER_ID = 2;

    int PR_ID = 3;
    int PM130_ID = 4;

    int OHMMETER_ID = 5;

    void read(Object... args);

    void write(Object... args);

    void resetAttempts();

    boolean thereAreAttempts();

    boolean needToRead();

    void setNeedToRead(boolean needToRead);
}