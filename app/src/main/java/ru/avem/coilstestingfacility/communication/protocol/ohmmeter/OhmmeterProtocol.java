package ru.avem.coilstestingfacility.communication.protocol.ohmmeter;

import android.content.Context;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.nio.ByteBuffer;
import java.util.Arrays;

import ru.avem.coilstestingfacility.communication.serial.SerialConnection;

public class OhmmeterProtocol {
    private static final int WRITE_TIMEOUT = 25;
    private static final int READ_TIMEOUT = 25;
    private static final int BAUD_RATE = 9600;

    private final SerialConnection mConnection;

    public OhmmeterProtocol(Context context, String productName) {
        mConnection = new SerialConnection(context, productName, BAUD_RATE,
                UsbSerialPort.DATABITS_8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE,
                WRITE_TIMEOUT, READ_TIMEOUT);
    }

    public SerialConnection getConnection() {
        return mConnection;
    }

    public synchronized boolean readData(ByteBuffer inputBuffer) {
        ByteBuffer outputBuffer = ByteBuffer.allocate(5)
                .put((byte) 0x55)
                .put((byte) 0x55)
                .put((byte) 0x00)
                .put((byte) 0x00)
                .put((byte) 0xAA);
        boolean status = false;
        mConnection.write(outputBuffer.array());
        byte inputArray[] = new byte[40];
        ByteBuffer finalBuffer = ByteBuffer.allocate(40);
        int attempt = 0;
        do {
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int frameSize = mConnection.read(inputArray);
            finalBuffer.put(inputArray, 0, frameSize);
        } while (finalBuffer.position() < 17 && (++attempt < 10));
        Log.i("TAG", "bytes: " + Arrays.toString(finalBuffer.array()));
        if (finalBuffer.position() == 17 && checkSum(finalBuffer.array())) {
            ((ByteBuffer) inputBuffer.clear()).put(finalBuffer.array(), 0, 17).flip().position(6);
            status = true;
        }
        return status;
    }

    private boolean checkSum(byte[] bytes) {
        int sum = 0;
        for (int i = 0; i < 16; i++) {
            sum += bytes[i];
            Log.i("TAG", "checkSum: " + sum + " " + bytes[i]);
        }
        if (bytes[16] >= 0) {
            sum &= 0xFF;
        }
        Log.i("TAG", "checkSum: " + sum + " " + bytes[16]);
        return sum == bytes[16];
    }
}