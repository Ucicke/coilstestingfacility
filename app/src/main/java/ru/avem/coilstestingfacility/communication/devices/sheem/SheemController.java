package ru.avem.coilstestingfacility.communication.devices.sheem;

import java.nio.ByteBuffer;
import java.util.Observer;

import ru.avem.coilstestingfacility.communication.devices.DeviceController;
import ru.avem.coilstestingfacility.communication.protocol.modbus.ModbusController;

public class SheemController implements DeviceController {
    private static final byte MODBUS_ADDRESS = 0x69;
    public static final short FILLING_LEVEL_REGISTER = 0;
    public static final short STATUS_REGISTER = 1;
    public static final short SWITCH_REGISTER = 2;

    private static final int CONVERT_BUFFER_SIZE = 2;
    private static final int NUM_OF_WORDS_IN_REGISTER = 4;
    private static final short NUM_OF_REGISTERS = 1 * NUM_OF_WORDS_IN_REGISTER;

    private SheemModel mModel;
    private ModbusController mModbusProtocol;
    private byte mAttempt = NUMBER_OF_ATTEMPTS;
    private boolean mNeedToReed;

    public SheemController(Observer observer, ModbusController controller) {
        mModel = new SheemModel(observer);
        mModbusProtocol = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusProtocol.readInputRegisters(
                    MODBUS_ADDRESS, FILLING_LEVEL_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
                mModel.setFillingLevel(inputBuffer.getShort());
                mModel.setStatus(inputBuffer.getShort());
                mModel.setSwitch(inputBuffer.getShort());
                mModel.setTimeKM(inputBuffer.getShort());
            } else {
                read(args);
            }
        } else {
            mModel.setResponding(false);
        }
    }

    @Override
    public void write(Object... args) {
        ByteBuffer mInputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusProtocol.writeSingleHoldingRegister(
                    MODBUS_ADDRESS, (short) args[0], shortToByteArray((short) args[1]),
                    mInputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
            } else {
                write(args);
            }
        } else {
            mModel.setResponding(false);
        }
    }

    @Override
    public void resetAttempts() {
        mAttempt = NUMBER_OF_ATTEMPTS;
    }

    @Override
    public boolean thereAreAttempts() {
        return mAttempt > 0;
    }

    private byte[] shortToByteArray(short s) {
        ByteBuffer convertBuffer = ByteBuffer.allocate(CONVERT_BUFFER_SIZE);
        convertBuffer.clear();
        return convertBuffer.putShort(s).array();
    }

    @Override
    public boolean needToRead() {
        return mNeedToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        mNeedToReed = needToRead;
    }
}