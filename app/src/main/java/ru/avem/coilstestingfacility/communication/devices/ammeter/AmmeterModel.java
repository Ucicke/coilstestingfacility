package ru.avem.coilstestingfacility.communication.devices.ammeter;

import java.util.Observable;
import java.util.Observer;

import static ru.avem.coilstestingfacility.communication.devices.DeviceController.AMMETER_ID;

public class AmmeterModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int I_PARAM = 1;

    AmmeterModel(Observer observer) {
        addObserver(observer);
    }

    public void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    void setI(float i) {
        notice(I_PARAM, i);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{AMMETER_ID, param, value});
    }
}