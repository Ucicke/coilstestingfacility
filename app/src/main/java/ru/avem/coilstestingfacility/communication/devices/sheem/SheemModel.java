package ru.avem.coilstestingfacility.communication.devices.sheem;

import java.util.Observable;
import java.util.Observer;

import static ru.avem.coilstestingfacility.communication.devices.DeviceController.SHEEM_ID;

public class SheemModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int FILLING_LEVEL_PARAM = 1;
    public static final int STATUS_PARAM = 2;
    public static final int SWITCH_PARAM = 3;
    public static final int TIME_KM_PARAM = 4;

    SheemModel(Observer observer) {
        addObserver(observer);
    }

    void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    void setFillingLevel(short fillingLevel) {
        notice(FILLING_LEVEL_PARAM, fillingLevel);
    }

    public void setStatus(short status) {
        notice(STATUS_PARAM, status);
    }

    public void setSwitch(short switchS) {
        notice(SWITCH_PARAM, switchS);
    }

    void setTimeKM(short timeKM) {
        notice(TIME_KM_PARAM, timeKM);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{SHEEM_ID, param, value});
    }
}