package ru.avem.coilstestingfacility.communication.devices.ohmmeter;

import java.util.Observable;
import java.util.Observer;

import ru.avem.coilstestingfacility.communication.devices.DeviceController;

public class OhmmeterModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int MODE_PARAM = 1;
    public static final int RESISTANCE_PARAM = 2;

    private static final byte NONE_MODE = 0;
    private static final byte L_MODE = 1;
    private static final byte C_MODE = 2;
    private static final byte R_MODE = 3;
    private static final byte DCR_MODE = 4;

    private short mMResistance;
    private float mDivider;
    private float mMultiplier;

    public OhmmeterModel(Observer observer) {
        addObserver(observer);
    }

    public void setResponding(boolean responding) {
        notice(RESPONDING_PARAM, responding);
    }

    public void setMMode(byte MMode) {
        notice(MODE_PARAM, MMode == DCR_MODE);
    }

    public void setMResistance(short MResistance) {
        mMResistance = MResistance;
    }

    public void setMScope(byte MScope) {
        int MScopeDot = MScope & 7;
        switch (MScopeDot) {
            case 0:
                mDivider = 1.f;
                break;
            case 1:
                mDivider = 10.f;
                break;
            case 2:
                mDivider = 100.f;
                break;
            case 3:
                mDivider = 1000.f;
                break;
            case 4:
                mDivider = 10000.f;
                break;
        }
        int MScopeUnit = (MScope & 0xF8) >> 3;
        switch (MScopeUnit) {
            case 0:
                mMultiplier = 0;
                break;
            case 1:
                mMultiplier = 1;
                break;
            case 2:
                mMultiplier = 1000;
                break;
            case 3:
                mMultiplier = 1000000;
                break;
        }
    }

    void setMStatus(byte MStatus) {
        if ((MStatus & 0x1F) == 3) {
            setResistance(0);
        } else {
            setResistance(mMResistance / mDivider * mMultiplier);
        }
    }

    private void setResistance(float resistance) {
        notice(RESISTANCE_PARAM, resistance);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{DeviceController.OHMMETER_ID, param, value});
    }
}