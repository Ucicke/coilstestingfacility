package ru.avem.coilstestingfacility.communication.devices.pr;

import java.nio.ByteBuffer;
import java.util.Observer;

import ru.avem.coilstestingfacility.communication.devices.DeviceController;
import ru.avem.coilstestingfacility.communication.protocol.modbus.ModbusController;

public class PRController implements DeviceController {
    private static final byte MODBUS_ADDRESS = 16;
    //    public static final short RESET_REGISTER = 512;
    public static final short SWITCH_TRANSF_KM_REGISTER = 513;
    public static final short DO_UP_REGISTER = 514;
    public static final short DO_DOWN_REGISTER = 515;
    public static final short RESET_TIME_REGISTER = 518;
    public static final short RESET_DOG_REGISTER = 519;
    public static final short ALARM_REGISTER = 768;
    public static final short AUTO_REGISTER = 769;
    public static final short UP_REGISTER = 770;
    public static final short DOWN_REGISTER = 771;
    public static final short CONC_1_REGISTER = 772;
    public static final short CONC_2_REGISTER = 773;
    public static final short TIME_REGISTER = 774;

    private static final int CONVERT_BUFFER_SIZE = 2;
    private static final int NUM_OF_WORDS_IN_REGISTER = 1;
    private static final short NUM_OF_REGISTERS = 7 * NUM_OF_WORDS_IN_REGISTER;

    private PRModel mModel;
    private ModbusController mModbusProtocol;
    private byte mAttempt = NUMBER_OF_ATTEMPTS;
    private boolean mNeedToReed;

    public PRController(Observer observer, ModbusController controller) {
        mModel = new PRModel(observer);
        mModbusProtocol = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusProtocol.readInputRegisters(
                    MODBUS_ADDRESS, ALARM_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
                mModel.setAlarm(inputBuffer.getShort() == 1);
                mModel.setAuto(inputBuffer.getShort() == 1);
                mModel.setUp(inputBuffer.getShort() == 1);
                mModel.setDown(inputBuffer.getShort() == 1);
                mModel.setConcUp(inputBuffer.getShort() == 1);
                mModel.setConcDown(inputBuffer.getShort() == 1);
                mModel.setTimerState(inputBuffer.getShort() == 1);
            } else {
                read(args);
            }
        } else {
            mModel.setResponding(false);
        }
    }

    @Override
    public void write(Object... args) {
        short register = (short) args[0];
        int numOfRegisters = (int) args[1];
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        ByteBuffer dataBuffer = ByteBuffer.allocate(2 * numOfRegisters);
        for (int i = 2; i < numOfRegisters + 2; i++) {
            dataBuffer.putShort((short) ((int) args[i]));
        }
        dataBuffer.flip();

        if (thereAreAttempts()) {
            mAttempt--;
            ModbusController.RequestStatus status = mModbusProtocol.writeMultipleHoldingRegisters(
                    MODBUS_ADDRESS, register, (short) numOfRegisters, dataBuffer, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                mModel.setResponding(true);
                resetAttempts();
            } else {
                write(args);
            }
        } else {
            mModel.setResponding(false);
        }
//        ByteBuffer mInputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
//        if (thereAreAttempts()) {
//            mAttempt--;
//            ModbusController.RequestStatus status = mModbusProtocol.writeSingleHoldingRegister(
//                    MODBUS_ADDRESS, (short) args[0], shortToByteArray((short) args[1]),
//                    mInputBuffer);
//            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
//                mModel.setResponding(true);
//                resetAttempts();
//            } else {
//                write(args);
//            }
//        } else {
//            mModel.setResponding(false);
//        }
    }

    @Override
    public void resetAttempts() {
        mAttempt = NUMBER_OF_ATTEMPTS;
    }

    @Override
    public boolean thereAreAttempts() {
        return mAttempt > 0;
    }

    private byte[] shortToByteArray(short s) {
        ByteBuffer convertBuffer = ByteBuffer.allocate(CONVERT_BUFFER_SIZE);
        convertBuffer.clear();
        return convertBuffer.putShort(s).array();
    }

    @Override
    public boolean needToRead() {
        return mNeedToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        mNeedToReed = needToRead;
    }
}