package ru.avem.coilstestingfacility.logging;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.github.mjdev.libaums.fs.FileSystem;
import com.github.mjdev.libaums.fs.UsbFile;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;

import ru.avem.coilstestingfacility.R;
import ru.avem.coilstestingfacility.database.model.Protocol;

import static android.content.Context.USB_SERVICE;

public class Logging {
    private static final String ACTION_USB_PERMISSION =
            "ru.avem.coilstestingfacility.USB_PERMISSION";

    public static void preview(Activity activity, Protocol protocol) {
        if (requestPermission(activity)) {
            new SaveTask(2, activity).execute(protocol);
        } else {
            Toast.makeText(activity, "Ошибка доступа. Дайте разрешение на запись.", Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean requestPermission(Activity activity) {
        boolean hasPermission = (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    112);
            return false;
        } else {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol";
            File storageDir = new File(path);
            if (!storageDir.exists() && !storageDir.mkdirs()) {
                return false;
            }
        }
        return true;
    }

    private static class SaveTask extends AsyncTask<Protocol, Void, String> {
        private ProgressDialog dialog;
        private int mType;
        private final Context mContext;

        SaveTask(int type, Context context) {
            mType = type;
            mContext = context;
            dialog = new ProgressDialog(context);
            if (mType == 1) {
                dialog.setMessage("Идёт сохранение...");
            } else if (mType == 2) {
                dialog.setMessage("Подождите...");
            }
            dialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected String doInBackground(Protocol... protocols) {
            String fileName = null;
            if (mType == 1) {
                fileName = writeWorkbookToMassStorage(protocols[0], mContext);
            } else if (mType == 2) {
                fileName = writeWorkbookToInternalStorage(protocols[0], mContext);
            }
            return fileName;
        }

        @Override
        protected void onPostExecute(String fileName) {
            super.onPostExecute(fileName);
            dialog.dismiss();
            if (mType == 1) {
                Toast.makeText(mContext, "Сохранено в " + fileName, Toast.LENGTH_SHORT).show();
            } else if (mType == 2) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                File file = new File(fileName);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri uri = FileProvider.getUriForFile(mContext,
                        mContext.getApplicationContext().getPackageName() + ".provider",
                        file);
                String mimeType = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                        "xlsx");
                intent.setDataAndType(uri, mimeType);
                mContext.startActivity(intent);
            }
        }
    }

    private static String writeWorkbookToMassStorage(Protocol protocol, Context context) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH-mm-ss-SSS");
        String fileName = "protocol-" + sdf.format(System.currentTimeMillis()) + ".xlsx";
        UsbMassStorageDevice[] massStorageDevices = UsbMassStorageDevice.getMassStorageDevices(context);
        UsbMassStorageDevice currentDevice = massStorageDevices[0];
        try {
            currentDevice.init();
            FileSystem currentFS = currentDevice.getPartitions().get(0).getFileSystem();

            UsbFile root = currentFS.getRootDirectory();
            UsbFile file = root.createFile(fileName);
            ByteArrayOutputStream out = convertProtocolToWorkbook(protocol, context);
            file.write(0, ByteBuffer.wrap(out.toByteArray()));
            file.close();
            fileName = currentFS.getVolumeLabel() + "/" + fileName;
            currentDevice.close();
        } catch (IOException e) {
            Log.e("TAG", "setup device error", e);
        }
        return fileName;
    }

    private static ByteArrayOutputStream convertProtocolToWorkbook(Protocol protocol, Context context) throws java.io.IOException {
        Resources res = context.getResources();
        InputStream inputStream = res.openRawResource(R.raw.template);
        Workbook wb = new XSSFWorkbook(inputStream);
        try {
            Sheet sheet = wb.getSheetAt(0);
            for (int i = 0; i < 1000; i++) {
                Row row = sheet.getRow(i);
                if (row != null) {
                    for (int j = 0; j < 1000; j++) {
                        Cell cell = row.getCell(j);
                        if (cell != null && (cell.getCellTypeEnum() == CellType.STRING)) {
                            switch (cell.getStringCellValue()) {
                                case "$PROTOCOL_NUMBER$":
                                    cell.setCellValue(protocol.getId() + "");
                                    break;
                                case "$OBJECT$":
                                    cell.setCellValue(protocol.getSubjectName() + "(" + protocol.getInductorName() + ")");
                                    break;
                                case "$SERIAL_NUMBER$":
                                    cell.setCellValue(protocol.getSerialNumber());
                                    break;
                                case "$U_SET_TC$":
                                    float specifiedUTC = (float) protocol.getSpecifiedUTC();
                                    if (specifiedUTC != -1) {
                                        cell.setCellValue(specifiedUTC + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_OBJ_TC$":
                                    float objectUTC = (float) protocol.getObjectUTC();
                                    if (objectUTC != -1) {
                                        cell.setCellValue(objectUTC + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_SET_TC$":
                                    float specifiedITC = (float) protocol.getSpecifiedITC();
                                    if (specifiedITC != -1) {
                                        cell.setCellValue(specifiedITC + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_OBJ_TC$":
                                    float objectITC = (float) protocol.getObjectITC();
                                    if (objectITC != -1) {
                                        cell.setCellValue(objectITC + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$TIME$":
                                    int experimentTime = protocol.getExperimentTime();
                                    cell.setCellValue(experimentTime + "");
                                    break;
                                case "$RESULT_TC$":
                                    if (protocol.getObjectITC() != -1) {
                                        if (protocol.isResultTC()) {
                                            cell.setCellValue("Успешно");
                                        } else {
                                            cell.setCellValue("Неуспешно");
                                        }
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_SET_ES$":
                                    float specifiedUES = (float) protocol.getSpecifiedUES();
                                    if (specifiedUES != -1) {
                                        cell.setCellValue(specifiedUES + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$U_OBJ_ES$":
                                    float objectUES = (float) protocol.getObjectUES();
                                    if (objectUES != -1) {
                                        cell.setCellValue(objectUES + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$I_SET_ES$":
                                    cell.setCellValue("< 5");
                                    break;
                                case "$I_OBJ_ES$":
                                    float objectIES = (float) protocol.getObjectIES();
                                    if (objectIES != -1) {
                                        if (protocol.isResultES()) {
                                            cell.setCellValue(objectIES + "");
                                        } else {
                                            cell.setCellValue("> 5");
                                        }
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$RESULT_ES$":
                                    if (protocol.getObjectIES() != -1) {
                                        boolean resultES = protocol.isResultES();
                                        if (resultES) {
                                            cell.setCellValue("Успешно");
                                        } else {
                                            cell.setCellValue("Неуспешно");
                                        }
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$R_SET$":
                                    float specifiedR = (float) protocol.getSpecifiedR();
                                    if (specifiedR != -1) {
                                        cell.setCellValue(specifiedR + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$R_OBJ$":
                                    float objectR = (float) protocol.getObjectR();
                                    if (objectR != -1) {
                                        cell.setCellValue(objectR + "");
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$R_RESULT$":
                                    if (protocol.getObjectR() != -1) {
                                        boolean resultR = protocol.isResultR();
                                        if (resultR) {
                                            cell.setCellValue("Успешно");
                                        } else {
                                            cell.setCellValue("Неуспешно");
                                        }
                                    } else {
                                        cell.setCellValue("");
                                    }
                                    break;
                                case "$POS1$":
                                    cell.setCellValue(protocol.getPosition1());
                                    break;
                                case "$POS2$":
                                    cell.setCellValue(protocol.getPosition2());
                                    break;
                                case "$POS1NAME$":
                                    cell.setCellValue("/" + protocol.getPosition1FullName() + "/");
                                    break;
                                case "$POS2NAME$":
                                    cell.setCellValue("/" + protocol.getPosition2FullName() + "/");
                                    break;
                                case "$DATE$":
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
                                    cell.setCellValue(sdf.format(protocol.getDate()));
                                    break;
                            }
                        }
                    }
                }
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                wb.write(out);
            } finally {
                out.close();
            }
            return out;
        } finally {
            wb.close();
        }
    }

    private static String writeWorkbookToInternalStorage(Protocol protocol, Context context) {
        clearDirectory(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol"));
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM(HH-mm-ss)");
        String fileName = "protocol-" + sdf.format(System.currentTimeMillis()) + ".xlsx";
        try {
            ByteArrayOutputStream out = convertProtocolToWorkbook(protocol, context);
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol", fileName);
            FileOutputStream fileOut = new FileOutputStream(file);
            out.writeTo(fileOut);
            fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/protocol/" + fileName;
            out.close();
            fileOut.close();
        } catch (IOException e) {
            Log.e("TAG", " error", e);
        }
        return fileName;
    }

    private static void clearDirectory(File directory) {
        for (File child : directory.listFiles()) {
            child.delete();
        }
    }

    public static void saveFileOnFlashMassStorage(Context context, Protocol protocol) {
        if (checkMassStorageConnection(context)) {
            new SaveTask(1, context).execute(protocol);
        } else {
            new AlertDialog.Builder(
                    context)
                    .setTitle("Нет подключения")
                    .setCancelable(false)
                    .setMessage("Подключите USB FLASH накопитель с файловой системой FAT32 и предоставьте доступ к нему")
                    .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .create()
                    .show();
        }
    }

    private static boolean checkMassStorageConnection(Context context) {
        UsbMassStorageDevice[] massStorageDevices = UsbMassStorageDevice.getMassStorageDevices(context);
        if (massStorageDevices.length != 1) {
            return false;
        } else {
            UsbManager usbManager = (UsbManager) context.getSystemService(USB_SERVICE);
            if (usbManager.hasPermission(massStorageDevices[0].getUsbDevice())) {
                return true;
            } else {
                PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
                usbManager.requestPermission(massStorageDevices[0].getUsbDevice(), pi);
                return false;
            }
        }
    }

    public static boolean isDeviceFlashMassStorage(UsbDevice device) {
        int interfaceCount = device.getInterfaceCount();
        for (int i = 0; i < interfaceCount; i++) {
            UsbInterface usbInterface = device.getInterface(i);
            int INTERFACE_SUBCLASS = 6;
            int INTERFACE_PROTOCOL = 80;
            if (usbInterface.getInterfaceClass() == UsbConstants.USB_CLASS_MASS_STORAGE
                    && usbInterface.getInterfaceSubclass() == INTERFACE_SUBCLASS
                    && usbInterface.getInterfaceProtocol() == INTERFACE_PROTOCOL) {
                return true;
            }
        }
        return false;
    }
}