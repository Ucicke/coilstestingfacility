package ru.avem.coilstestingfacility.main.controller;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import java.util.List;
import java.util.Objects;

import ru.avem.coilstestingfacility.communication.devices.DevicesController;
import ru.avem.coilstestingfacility.database.model.DatabaseAdapter;
import ru.avem.coilstestingfacility.database.model.Protocol;
import ru.avem.coilstestingfacility.database.model.Subject;
import ru.avem.coilstestingfacility.logging.Logging;
import ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel;

import static android.content.Context.USB_SERVICE;
import static ru.avem.coilstestingfacility.logging.Logging.isDeviceFlashMassStorage;

public class CoilsTestingFacilityController {
    public static final int OK_RESULT_TC = 0;
    public static final int CONC_UP_RESULT_TC = 1;
    public static final int BREAKDOWN_RESULT_TC = 2;
    public static final int AMPERAGE_IS_NOT_SPECIFIED = 4;

    public static final int OK_RESULT_ES = 0;
    public static final int BREAKDOWN_RESULT_ES = 1;
    public static final int CONC_UP_RESULT_ES = 2;

    private static final String ACTION_USB_PERMISSION =
            "ru.avem.coilstestingfacility.USB_PERMISSION";
    private static final String ACTION_USB_ATTACHED =
            "android.hardware.usb.action.USB_DEVICE_ATTACHED";
    private static final String ACTION_USB_DETACHED =
            "android.hardware.usb.action.USB_DEVICE_DETACHED";

    private final CoilsTestingFacilityModel mModel = CoilsTestingFacilityModel.getInstance();

    private DatabaseAdapter mDatabaseAdapter;
    private DevicesController mDevicesController;

    public CoilsTestingFacilityController(Context context) {
        mDatabaseAdapter = new DatabaseAdapter(context);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(ACTION_USB_ATTACHED);
        filter.addAction(ACTION_USB_DETACHED);
        BroadcastReceiver usbReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (ACTION_USB_PERMISSION.equals(action)) {
                    synchronized (this) {
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            if (device != null) {
                                if (Objects.equals(device.getProductName(), "CP2103 USB to RS-485")) {
                                    mDevicesController.connectMainBus();
                                } else if (Objects.equals(device.getProductName(), "CP2102 USB to UART Bridge Controller")) {
                                    mDevicesController.connectOhmmeterBus();
                                } else if (isDeviceFlashMassStorage(device)) {
                                    Logging.saveFileOnFlashMassStorage(context, mModel.getProtocolForInteraction());
                                }
                            }
                        }
                    }
                } else if (ACTION_USB_DETACHED.equals(action) || ACTION_USB_ATTACHED.equals(action)) {
                    synchronized (this) {
                        mDevicesController.disconnectMainBus();
                        mDevicesController.disconnectOhmmeterBus();
                        mDevicesController.connectMainBus();
                        mDevicesController.connectOhmmeterBus();
                        if (ACTION_USB_ATTACHED.equals(action) && isDeviceFlashMassStorage(device)) {
                            synchronized (this) {
                                UsbManager usbManager = (UsbManager) context.getSystemService(USB_SERVICE);
                                PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
                                if (usbManager != null) {
                                    usbManager.requestPermission(device, pi);
                                }
                            }
                        }
                    }
                }
            }
        };
        context.registerReceiver(usbReceiver, filter);

        mDevicesController = new DevicesController(context, mModel);
    }

    public void connectSheem() {
        mDevicesController.connectSheem();
    }

    public void connectVoltmeter() {
        mDevicesController.connectVoltmeter();
    }

    public void connectAmmeter() {
        mDevicesController.connectAmmeter();
    }

    public void connectPR() {
        mDevicesController.connectPR();
    }

    public void connectPM130() {
        mDevicesController.connectPM130();
    }

    public void connectOhmmeter() {
        mDevicesController.connectOhmmeter();
    }

    public void initTCDevices() {
        mDevicesController.initTCDevices();
    }

    public void initESDevices() {
        mDevicesController.initESDevices();
    }

    public void initOHMDevices() {
        mDevicesController.initOHMDevices();
    }

    public void diversifyDevices() {
        mDevicesController.diversifyDevices();
    }

    public int startTestTurnClosure() {
        mModel.setExperimentEndTC(false);
        initSheemState();
        mDevicesController.onKMSheem();
        int result = increaseVoltage();
        initSheemState();
        return result;
    }

    private int increaseVoltage() {
        double specifiedUTC = mModel.getSpecifiedUTC();
        /*double specifiedITCPlus = mModel.getSpecifiedUTCPlus();*/
        int i = 2400;

        if (specifiedUTC < 2000) {
            while ((mModel.getOnlineUTC() <= specifiedUTC) && (i <= 4500)/* && (mModel.getOnlineITC() <= specifiedITCPlus)*/) {
                mDevicesController.setFillingLevelSheem((short) (i += 10));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            while ((mModel.getOnlineUTC() <= (specifiedUTC * 0.9)) && (i <= 4500)/* && (mModel.getOnlineITC() <= specifiedITCPlus)*/) {
                mDevicesController.setFillingLevelSheem((short) (i += 15));
            }
            while ((mModel.getOnlineUTC() < (specifiedUTC)) && (i <= 4500)/* && (mModel.getOnlineITC() <= specifiedITCPlus)*/) {
                mDevicesController.setFillingLevelSheem((short) (i += 2));
            }
        }

        if ((i > 4500) /*|| (mModel.getOnlineITC() > specifiedITCPlus)*/) {
            return CONC_UP_RESULT_TC;
        }

        int t = mModel.getExperimentTime() * 10;
        while (/*(mModel.getOnlineITC() <= specifiedITCPlus) &&*/ (--t >= 0)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {
            }
            mModel.setTimeLeftTC(t / 10);
        }
        mModel.saveTCResult();


        /*if (mModel.getObjectITC() <= specifiedITCPlus) {*/
            return OK_RESULT_TC;
        /*} else {
            return BREAKDOWN_RESULT_TC;
        }*/
    }

    private void initSheemState() {
        mDevicesController.setFillingLevelSheem((short) 2400);
        mDevicesController.offKMSheem();
    }

    public int startTeachingTurnClosure() {
        mModel.setExperimentEndTC(false);
        initSheemState();
        mDevicesController.onKMSheem();
        int result = increaseTeachingVoltage();
        initSheemState();
        return result;
    }

    private int increaseTeachingVoltage() {
        double specifiedUTC = mModel.getSpecifiedUTC();
        int i = 2400;

        if (specifiedUTC < 2000) {
            while ((mModel.getOnlineUTC() <= specifiedUTC) && (i <= 4500)) {
                mDevicesController.setFillingLevelSheem((short) (i += 10));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            while ((mModel.getOnlineUTC() <= (specifiedUTC * 0.9)) && (i <= 4500)) {
                mDevicesController.setFillingLevelSheem((short) (i += 15));
            }
            while ((mModel.getOnlineUTC() < (specifiedUTC)) && (i <= 4500)) {
                mDevicesController.setFillingLevelSheem((short) (i += 2));
            }
        }
        mModel.saveTCResult();

        if (i > 4500) {
            return 1;
        } else {
            if ((mModel.getObjectUTC() < mModel.getSpecifiedUTCPlus())
                    && (mModel.getObjectUTC() > mModel.getSpecifiedUTCMinus())) {
                mModel.setSpecifiedITC(mModel.getObjectITC());
                mModel.setSpecifiedITCMinus(mModel.getObjectITC() * 0.9);
                mModel.setSpecifiedITCPlus(mModel.getObjectITC() * 1.1);
                return 0;
            } else {
                return 2;
            }
        }
    }


    public int startTestElectricStrength() {
        try {
            mDevicesController.resetTimer();
            initLATR();
            mModel.setExperimentEndES(false);

            double uzd = mModel.getSpecifiedUES();
            double uzdMin = uzd - 50;
            double uzdMax = uzd + 50;

            if (uzd < 2500) {
                mDevicesController.startWithOnTransfKmLATR();
                uzdMin = uzd - 10;
                uzdMax = uzd + 10;
            } else {
                mDevicesController.startUpLATR();
            }

            while (!mModel.isOnlineConcUpES() && (mModel.getOnlineUES() < (uzd * 0.9)) && !mModel.isOnlineAlarmES()) {
                Thread.sleep(100);
            }
            mDevicesController.stopLATR();
            if (mModel.isOnlineAlarmES()) {
                mModel.setExperimentEndES(true);
                return BREAKDOWN_RESULT_ES;
            }
            if (mModel.isOnlineConcUpES()) {
                mModel.setExperimentEndES(true);
                return CONC_UP_RESULT_ES;
            }
            while (((mModel.getOnlineUES() < uzdMin) || (mModel.getOnlineUES() > uzdMax)) && !mModel.isOnlineAlarmES()) {
                if (mModel.getOnlineUES() < uzdMin) {
                    mDevicesController.startUpLATR();
                } else if (mModel.getOnlineUES() > uzdMax) {
                    mDevicesController.startDownLATR();
                }
                Thread.sleep(25);
                mDevicesController.stopLATR();
                Thread.sleep(50);
            }
            mDevicesController.stopLATR();
            if (mModel.isOnlineAlarmES()) {
                mModel.setExperimentEndES(true);
                return BREAKDOWN_RESULT_ES;
            }
            int i = 600;
            while (!mModel.isOnlineAlarmES() && (--i >= 0)) {
                Thread.sleep(100);
                mModel.setTimeLeftES(i / 10);
            }
            mModel.setExperimentEndES(true);
            if (mModel.isOnlineAlarmES()) {
                return BREAKDOWN_RESULT_ES;
            }
            if (!returnToStart()) {
                return BREAKDOWN_RESULT_ES;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return OK_RESULT_ES;
    }

    private void initLATR() {
        if (!mModel.isOnlineConcDownES()) {
            returnToStart();
        }
        mDevicesController.offTransfKmLATR();
    }

    private boolean returnToStart() {
        mDevicesController.startDownLATR();
        while (!mModel.isOnlineConcDownES() && !mModel.isOnlineAlarmES()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mDevicesController.stopWithOffTransfKmLATR();
        return !mModel.isOnlineAlarmES();
    }


    public int startTestResistance() {
        mDevicesController.readOhmmeter();
        mModel.setObjectR(mModel.getOnlineR());
        boolean result = (mModel.getObjectR() > mModel.getSpecifiedRMinus()) &&
                (mModel.getObjectR() < mModel.getSpecifiedRPlus());
        return result ? 0 : 1;
    }


    public boolean findSerialNumberInDB(String serialNumber) {
        boolean result;
        mDatabaseAdapter.open();
        Protocol protocol = mDatabaseAdapter.getProtocol(serialNumber);
        if (protocol != null) {
            mModel.setProtocol(protocol);
            mModel.fillSpecifiedFields();
            result = true;
        } else {
            protocol = new Protocol(serialNumber);
            mModel.setProtocol(protocol);
            result = false;
        }
        mDatabaseAdapter.close();
        return result;
    }

    public void saveProtocolInDB() {
        mDatabaseAdapter.open();
        if (mModel.getProtocolId() > 0) {
            mDatabaseAdapter.updateProtocol(mModel.getProtocol());
        } else {
            mModel.saveProtocolId(mDatabaseAdapter.insertProtocol(mModel.getProtocol()));
        }
        mDatabaseAdapter.close();
    }

    public List<Subject> getAllSubjectsFromDB() {
        List<Subject> subjects;
        mDatabaseAdapter.open();
        subjects = mDatabaseAdapter.getSubjects();
        mDatabaseAdapter.close();
        return subjects;
    }

    public List<Protocol> getAllProtocolsFromDB() {
        List<Protocol> protocols;
        mDatabaseAdapter.open();
        protocols = mDatabaseAdapter.getProtocols();
        mDatabaseAdapter.close();
        return protocols;
    }
}