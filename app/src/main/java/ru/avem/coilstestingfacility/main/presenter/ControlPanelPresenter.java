package ru.avem.coilstestingfacility.main.presenter;

import ru.avem.coilstestingfacility.main.view.ControlPanelView;

public class ControlPanelPresenter {
    private final ControlPanelView mView;

    public ControlPanelPresenter(ControlPanelView view) {
        mView = view;
    }

    public void protectionStatusClicked() {
    }

    public void subjectClicked() {
    }

    public void eventsClicked() {
    }

    public void devicesStateClicked() {
        mView.displayDevicesStatePanel();
    }

    public void dataTransferClicked() {
    }

    public void switcherClicked() {
        if (mView.isControlPanelDisplayed()) {
            mView.hideControlPanel();
        } else {
            mView.displayControlPanel();
        }
    }
}
