package ru.avem.coilstestingfacility.main.view;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.avem.coilstestingfacility.R;
import ru.avem.coilstestingfacility.database.model.Inductor;
import ru.avem.coilstestingfacility.database.model.Protocol;
import ru.avem.coilstestingfacility.database.model.Subject;
import ru.avem.coilstestingfacility.logging.Logging;
import ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController;
import ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel;
import ru.avem.coilstestingfacility.main.presenter.ControlPanelPresenter;

import static android.text.TextUtils.isEmpty;
import static ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController.AMPERAGE_IS_NOT_SPECIFIED;
import static ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController.BREAKDOWN_RESULT_ES;
import static ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController.BREAKDOWN_RESULT_TC;
import static ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController.CONC_UP_RESULT_ES;
import static ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController.CONC_UP_RESULT_TC;
import static ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController.OK_RESULT_ES;
import static ru.avem.coilstestingfacility.main.controller.CoilsTestingFacilityController.OK_RESULT_TC;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.AMMETER_RESPONDING_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.EXPERIMENT_SELECTED_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.OBJECT_I_ES_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.OBJECT_I_TC_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.OBJECT_R_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.OBJECT_U_ES_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.OBJECT_U_TC_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.OHMMETER_RESPONDING_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.PM130_RESPONDING_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.PR_RESPONDING_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.SHEEM_RESPONDING_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.SPECIFIED_I_TC_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.SPECIFIED_R_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.SPECIFIED_U_ES_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.SPECIFIED_U_TC_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.TIME_LEFT_ES_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.TIME_LEFT_TC_PARAM;
import static ru.avem.coilstestingfacility.main.model.CoilsTestingFacilityModel.VOLTMETER_RESPONDING_PARAM;
import static ru.avem.coilstestingfacility.utils.Utils.setSpinnerAdapter;
import static ru.avem.coilstestingfacility.utils.Visibility.FullScreenCall;
import static ru.avem.coilstestingfacility.utils.Visibility.addTabToTabHost;
import static ru.avem.coilstestingfacility.utils.Visibility.disableView;
import static ru.avem.coilstestingfacility.utils.Visibility.enableView;
import static ru.avem.coilstestingfacility.utils.Visibility.setEmptyText;
import static ru.avem.coilstestingfacility.utils.Visibility.setViewAndChildrenVisibility;
import static ru.avem.coilstestingfacility.utils.Visibility.switchTabState;

public class CoilsTestingFacilityActivity extends AppCompatActivity implements Observer,
        ControlPanelView {
    //region Константы вкладок
    private static final String OBJECT_TAB_TAG = "Object";
    private static final int OBJECT_VIEW_ID = R.id.tab_object;
    private static final String OBJECT_TAB_LABEL = "Объект";
    @SuppressWarnings("unused")
    private static final int OBJECT_TAB_INDEX = 0;

    private static final String EXPERIMENTS_TAB_TAG = "Experiments";
    private static final int EXPERIMENTS_VIEW_ID = R.id.tab_experiments;
    private static final String EXPERIMENTS_TAB_LABEL = "Опыты";
    private static final int EXPERIMENTS_TAB_INDEX = 1;

    private static final String RESULTS_TAB_TAG = "Results";
    private static final int RESULTS_VIEW_ID = R.id.tab_results;
    private static final String RESULTS_TAB_LABEL = "Результаты";
    private static final int RESULT_TAB_INDEX = 2;

    private static final String PROTOCOL_TAB_TAG = "Protocol";
    private static final int PROTOCOL_VIEW_ID = R.id.tab_protocol;
    private static final String PROTOCOL_TAB_LABEL = "Протокол";
    private static final int PROTOCOL_TAB_INDEX = 3;
    //endregion

    //region Константные поля
    private final CoilsTestingFacilityModel mModel = CoilsTestingFacilityModel.getInstance();
    private final Handler mHandler = new Handler();
    private final ControlPanelPresenter mControlPanelPresenter = new ControlPanelPresenter(this);
    //endregion

    //region Поля
    private CoilsTestingFacilityController mController;
    private boolean isFirstTime = true;
    //endregion

    //region BindView
    @BindView(R.id.main_title)
    TextView mMainTitle;
    @BindView(R.id.objectTitle)
    TextView mObjectTitle;
    @BindView(R.id.numberTitle)
    TextView mNumberTitle;


    @BindView(R.id.tab_host)
    TabHost mTabHost;
    @BindView(android.R.id.tabs)
    TabWidget mTabs;


    @BindView(R.id.option_selection_layout)
    LinearLayout mOptionSelectionLayout;
    @BindView(R.id.switcher)
    ToggleButton mSwitcher;


    @BindView(R.id.devices_state_panel)
    LinearLayout mDevicesStatePanel;

    @BindView(R.id.sheem)
    LinearLayout mSheem;
    @BindView(R.id.sheem_responding)
    TextView mSheemResponding;
    @BindView(R.id.sheem_reconnect)
    Button mSheemReconnect;

    @BindView(R.id.voltmeter)
    LinearLayout mVoltmeter;
    @BindView(R.id.voltmeter_responding)
    TextView mVoltmeterResponding;
    @BindView(R.id.voltmeter_reconnect)
    Button mVoltmeterReconnect;

    @BindView(R.id.ammeter)
    LinearLayout mAmmeter;
    @BindView(R.id.ammeter_responding)
    TextView mAmmeterResponding;
    @BindView(R.id.ammeter_reconnect)
    Button mAmmeterReconnect;

    @BindView(R.id.pr)
    LinearLayout mPr;
    @BindView(R.id.pr_responding)
    TextView mPRResponding;
    @BindView(R.id.pr_reconnect)
    Button mPRReconnect;

    @BindView(R.id.pm130)
    LinearLayout mPM130;
    @BindView(R.id.pm130_responding)
    TextView mPM130Responding;
    @BindView(R.id.pm130_reconnect)
    TextView mPM130Reconnect;

    @BindView(R.id.ohmmeter)
    LinearLayout mOhmmeter;
    @BindView(R.id.ohmmeter_responding)
    TextView mOhmmeterResponding;
    @BindView(R.id.ohmmeter_reconnect)
    TextView mOhmmeterReconnect;


    @BindView(R.id.factory_number)
    EditText mFactoryNumber;
    @BindView(R.id.factory_number_enter)
    Button mFactoryNumberEnter;

    @BindView(R.id.objects_selector_title)
    TextView mObjectsSelectorTitle;
    @BindView(R.id.objects_selector)
    Spinner mObjectsSelector;

    @BindView(R.id.inductors_selector_title)
    TextView mInductorsSelectorTitle;
    @BindView(R.id.inductors_selector)
    Spinner mInductorsSelector;

    @BindView(R.id.object_enter)
    Button mObjectEnter;
    @BindView(R.id.object_cancel)
    Button mObjectCancel;
    @BindView(R.id.object_next)
    Button mObjectNext;


    @BindView(R.id.start_experiment)
    Button mStartExperiment;


    @BindView(R.id.experiment1)
    CheckBox mExperiment1;
    @BindView(R.id.experiment2)
    CheckBox mExperiment2;
    @BindView(R.id.experiment3)
    CheckBox mExperiment3;


    @BindView(R.id.result_selector)
    Spinner mResultSelector;

    @BindView(R.id.table_TC)
    TableLayout mTableTC;
    @BindView(R.id.specified_U_TC)
    TextView mSpecifiedUTC;
    @BindView(R.id.object_U_TC)
    TextView mObjectUTC;
    @BindView(R.id.specified_I_TC)
    TextView mSpecifiedITC;
    @BindView(R.id.object_I_TC)
    TextView mObjectITC;
    @BindView(R.id.experiment_time_TC)
    TextView mExperimentTimeTC;
    @BindView(R.id.result_TC)
    TextView mResultTC;

    @BindView(R.id.table_ES)
    TableLayout mTableES;
    @BindView(R.id.specified_U_ES)
    TextView mSpecifiedUES;
    @BindView(R.id.object_U_ES)
    TextView mObjectUES;
    @BindView(R.id.object_I_ES)
    TextView mObjectIES;
    @BindView(R.id.experiment_time_ES)
    TextView mExperimentTimeES;
    @BindView(R.id.result_ES)
    TextView mResultES;

    @BindView(R.id.table_R)
    TableLayout mTableR;
    @BindView(R.id.specified_R)
    TextView mSpecifiedR;
    @BindView(R.id.object_R)
    TextView mObjectR;
    @BindView(R.id.result_R)
    TextView mResultR;

    @BindView(R.id.experiment1_btn)
    Button mExperiment1Btn;
    @BindView(R.id.experiment2_btn)
    Button mExperiment2Btn;
    @BindView(R.id.experiment3_btn)
    Button mExperiment3Btn;
    @BindView(R.id.connection_warning)
    LinearLayout mConnectionWarning;


    @BindView(R.id.review_layout)
    GridLayout mReviewLayout;
    @BindView(R.id.save_layout)
    GridLayout mSaveLayout;

    @BindView(R.id.protocols)
    Spinner mProtocols;

    @BindView(R.id.position1)
    Spinner mPosition1;
    @BindView(R.id.position1_number)
    EditText mPosition1Number;
    @BindView(R.id.position1_full_name)
    EditText mPosition1FullName;
    @BindView(R.id.position2)
    Spinner mPosition2;
    @BindView(R.id.position2_number)
    EditText mPosition2Number;
    @BindView(R.id.position2_full_name)
    EditText mPosition2FullName;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_coils_testing_facility);

        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");
        ButterKnife.bind(this);

        mModel.addObserver(this);
        mController = new CoilsTestingFacilityController(this);
        initializeViews();
    }

    private void initializeViews() {
        initializeTabHost();
        deinitializeSelectors();
        initializeExperimentsSelectors();
        hideDevicesStates();
        initializeResultSelector();
        initializeProtocolsSelector();
    }

    private void initializeTabHost() {
        mTabHost.setup();
        addTabToTabHost(mTabHost, OBJECT_TAB_TAG, OBJECT_VIEW_ID, OBJECT_TAB_LABEL);
        addTabToTabHost(mTabHost, EXPERIMENTS_TAB_TAG, EXPERIMENTS_VIEW_ID, EXPERIMENTS_TAB_LABEL);
        addTabToTabHost(mTabHost, RESULTS_TAB_TAG, RESULTS_VIEW_ID, RESULTS_TAB_LABEL);
        addTabToTabHost(mTabHost, PROTOCOL_TAB_TAG, PROTOCOL_VIEW_ID, PROTOCOL_TAB_LABEL);
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tag) {
                switch (tag) {
                    case EXPERIMENTS_TAB_TAG:
                        mModel.removeAllExperiments();
                        mExperiment1.setChecked(false);
                        mExperiment2.setChecked(false);
                        mExperiment3.setChecked(false);
                        break;
                    case RESULTS_TAB_TAG:
                        mConnectionWarning.setVisibility(View.GONE);
                        break;
                    case PROTOCOL_TAB_TAG:
                        if (mModel.isNeedToSave()) {
                            mReviewLayout.setVisibility(View.GONE);
                            mSaveLayout.setVisibility(View.VISIBLE);
                        } else {
                            showProtocols();
                        }
                        break;
                }
            }
        });
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            TextView tabTextView = mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tabTextView.setTextColor(Color.parseColor("#000000"));
        }
    }

    private void showProtocols() {
        ArrayAdapter<Protocol> protocolArrayAdapter =
                new ArrayAdapter<>(CoilsTestingFacilityActivity.this,
                        android.R.layout.simple_spinner_item,
                        mController.getAllProtocolsFromDB());
        protocolArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mProtocols.setAdapter(protocolArrayAdapter);
        mSaveLayout.setVisibility(View.GONE);
        mReviewLayout.setVisibility(View.VISIBLE);
    }

    private void deinitializeSelectors() {
        setViewAndChildrenVisibility(mObjectsSelectorTitle, View.GONE);
        setViewAndChildrenVisibility(mObjectsSelector, View.GONE);
        setViewAndChildrenVisibility(mInductorsSelectorTitle, View.GONE);
        setViewAndChildrenVisibility(mInductorsSelector, View.GONE);
        setViewAndChildrenVisibility(mObjectEnter, View.GONE);

        setSpinnerAdapter(this, mObjectsSelector, new ArrayList<>());
        setSpinnerAdapter(this, mInductorsSelector, new ArrayList<>());
    }

    private void initializeExperimentsSelectors() {
        disableView(mExperiment1);
        disableView(mExperiment2);
        disableView(mExperiment3);
        mExperiment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mModel.addExperiment("1");
                } else {
                    mModel.removeExperiment("1");
                }
            }
        });
        mExperiment2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mModel.addExperiment("2");
                } else {
                    mModel.removeExperiment("2");
                }
            }
        });
        mExperiment3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mModel.addExperiment("3");
                } else {
                    mModel.removeExperiment("3");
                }
            }
        });
    }

    private void hideDevicesStates() {
        setViewAndChildrenVisibility(mSheem, View.GONE);
        setViewAndChildrenVisibility(mVoltmeter, View.GONE);
        setViewAndChildrenVisibility(mAmmeter, View.GONE);
        setViewAndChildrenVisibility(mPr, View.GONE);
        setViewAndChildrenVisibility(mPM130, View.GONE);
        setViewAndChildrenVisibility(mOhmmeter, View.GONE);
    }

    private void initializeResultSelector() {
        mResultSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        selectFirstResult();
                        break;
                    case 1:
                        selectSecondResult();
                        break;
                    case 2:
                        selectThirdResult();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initializeProtocolsSelector() {
        mProtocols.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mModel.setProtocolForInteraction((Protocol) mProtocols.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void selectFirstResult() {
        mTableTC.setVisibility(View.VISIBLE);
        mTableES.setVisibility(View.GONE);
        mTableR.setVisibility(View.GONE);
    }

    private void selectSecondResult() {
        mTableTC.setVisibility(View.GONE);
        mTableES.setVisibility(View.VISIBLE);
        mTableR.setVisibility(View.GONE);
    }

    private void selectThirdResult() {
        mTableTC.setVisibility(View.GONE);
        mTableES.setVisibility(View.GONE);
        mTableR.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FullScreenCall(this);
    }

    @Override
    public void update(Observable observable, final Object values) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                int param = (int) (((Object[]) values)[0]);
                Object value = ((Object[]) values)[1];
                switch (param) {
                    case SHEEM_RESPONDING_PARAM:
                        if ((boolean) value) {
                            mSheemResponding.setText("ШИМ ОК");
                            mSheemReconnect.setVisibility(View.GONE);
                        } else {
                            mSheemResponding.setText("ШИМ не отвечает");
                            mSheemReconnect.setVisibility(View.VISIBLE);
                        }
                        break;
                    case VOLTMETER_RESPONDING_PARAM:
                        if ((boolean) value) {
                            mVoltmeterResponding.setText("Вольтметр ОК");
                            mVoltmeterReconnect.setVisibility(View.GONE);
                        } else {
                            mVoltmeterResponding.setText("Вольтметр не отвечает");
                            mVoltmeterReconnect.setVisibility(View.VISIBLE);
                        }
                        break;
                    case AMMETER_RESPONDING_PARAM:
                        if ((boolean) value) {
                            mAmmeterResponding.setText("Амперметр ОК");
                            mAmmeterReconnect.setVisibility(View.GONE);
                        } else {
                            mAmmeterResponding.setText("Амперметр не отвечает");
                            mAmmeterReconnect.setVisibility(View.VISIBLE);
                        }
                        break;
                    case PR_RESPONDING_PARAM:
                        if ((boolean) value) {
                            mPRResponding.setText("Программируемое реле ОК");
                            mPRReconnect.setVisibility(View.GONE);
                        } else {
                            mPRResponding.setText("Программируемое реле не отвечает");
                            mPRReconnect.setVisibility(View.VISIBLE);
                        }
                        break;
                    case PM130_RESPONDING_PARAM:
                        if ((boolean) value) {
                            mPM130Responding.setText(R.string.pm130_ok);
                            mPM130Reconnect.setVisibility(View.GONE);
                        } else {
                            mPM130Responding.setText(R.string.pm130_not_ok);
                            mPM130Reconnect.setVisibility(View.VISIBLE);
                        }
                        break;
                    case OHMMETER_RESPONDING_PARAM:
                        if ((boolean) value) {
                            mOhmmeterResponding.setText("Омметр ОК");
                            mOhmmeterReconnect.setVisibility(View.GONE);
                        } else {
                            mOhmmeterResponding.setText("Омметр не отвечает");
                            mOhmmeterReconnect.setVisibility(View.VISIBLE);
                        }
                        break;
                    case EXPERIMENT_SELECTED_PARAM:
                        if ((boolean) value) {
                            enableView(mStartExperiment);
                        } else {
                            disableView(mStartExperiment);
                        }
                        break;
                    case SPECIFIED_U_TC_PARAM:
                        if ((double) value != -1) {
                            mSpecifiedUTC.setText(String.format("%s", (double) value));
                        }
                        break;
                    case OBJECT_U_TC_PARAM:
                        if ((float) value != -1) {
                            mObjectUTC.setText(String.format("%s", (float) value));
                        }
                        break;
                    case SPECIFIED_I_TC_PARAM:
                        if ((double) value != -1) {
                            mSpecifiedITC.setText(String.format("%s", (double) value));
                        }
                        break;
                    case OBJECT_I_TC_PARAM:
                        if ((float) value != -1) {
                            mObjectITC.setText(String.format("%s", (float) value));
                        }
                        break;
                    case TIME_LEFT_TC_PARAM:
                        mExperimentTimeTC.setText(String.format("%s", (int) value));
                        break;
                    case SPECIFIED_U_ES_PARAM:
                        if ((double) value != -1) {
                            mSpecifiedUES.setText(String.format("%s", (double) value));
                        }
                        break;
                    case OBJECT_U_ES_PARAM:
                        if ((float) value != -1) {
                            mObjectUES.setText(String.format("%s", (float) value));
                        }
                        break;
                    case OBJECT_I_ES_PARAM:
                        if ((float) value != -1) {
                            mObjectIES.setText(String.format("%s", (float) value));
                        }
                        break;
                    case TIME_LEFT_ES_PARAM:
                        mExperimentTimeES.setText(String.format("%s", (int) value));
                        break;
                    case SPECIFIED_R_PARAM:
                        if ((double) value != -1) {
                            mSpecifiedR.setText(String.format("%s", (double) value));
                        }
                        break;
                    case OBJECT_R_PARAM:
                        if ((float) value != -1) {
                            mObjectR.setText(String.format("%s", (float) value));
                        }
                        break;
                }
            }
        });
    }

    @OnClick({R.id.exit, R.id.factory_number_enter, R.id.object_enter, R.id.object_cancel, R.id.object_next,
            R.id.start_experiment,
            R.id.sheem_reconnect, R.id.voltmeter_reconnect, R.id.ammeter_reconnect,
            R.id.pr_reconnect, R.id.pm130_reconnect, R.id.ohmmeter_reconnect,
            R.id.protection_status, R.id.subject, R.id.events, R.id.devices_state, R.id.data_transfer, R.id.switcher,
            R.id.experiment1_btn, R.id.experiment3_btn, R.id.experiment2_btn,
            R.id.retry,
            R.id.save, R.id.preview, R.id.save_on_flash})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exit:
                finish();
            case R.id.factory_number_enter:
                String factoryNumber = mFactoryNumber.getText().toString();
                if (!factoryNumber.equals("")) {
                    if (mController.findSerialNumberInDB(factoryNumber)) {
                        mObjectTitle.setText(String.format("Объект: %s(%s)", mModel.getSubjectName(),
                                mModel.getInductorName()));
                        disableView(mObjectsSelector);
                        disableView(mInductorsSelector);
                        disableView(mObjectEnter);
                        setViewAndChildrenVisibility(mObjectsSelectorTitle, View.GONE);
                        mObjectCancel.setVisibility(View.VISIBLE);
                        mObjectNext.setVisibility(View.VISIBLE);
                    } else {
                        enableView(mObjectEnter);
                        initializeSelectors();
                    }
                    disableView(mFactoryNumber);
                    disableView(mFactoryNumberEnter);
                    mNumberTitle.setText(String.format("№ %s", factoryNumber));
                } else {
                    Toast.makeText(this, "Необходимо ввести заводской номер",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.object_enter:
                mObjectTitle.setText(String.format("Объект: %s(%s)",
                        mObjectsSelector.getSelectedItem(), mInductorsSelector.getSelectedItem()));
                disableView(mObjectsSelector);
                disableView(mInductorsSelector);
                disableView(mObjectEnter);
                mObjectCancel.setVisibility(View.VISIBLE);
                mObjectNext.setVisibility(View.VISIBLE);
                mModel.saveSubjectName((Subject) mObjectsSelector.getSelectedItem());
                mModel.saveInductor((Inductor) mInductorsSelector.getSelectedItem());
                break;
            case R.id.object_cancel:
                mModel.setProtocol(null);
                setEmptyText(mObjectTitle);
                setEmptyText(mNumberTitle);
                disableView(mExperiment1);
                disableView(mExperiment2);
                disableView(mExperiment3);
                enableView(mFactoryNumber);
                enableView(mFactoryNumberEnter);
                mObjectCancel.setVisibility(View.GONE);
                mObjectNext.setVisibility(View.GONE);
                isFirstTime = true;
                setViewAndChildrenVisibility(mInductorsSelectorTitle, View.VISIBLE);
                enableView(mObjectsSelector);
                enableView(mInductorsSelector);
                deinitializeSelectors();
                break;
            case R.id.object_next:
                enableView(mExperiment1);
                enableView(mExperiment2);
                enableView(mExperiment3);
                mTabHost.setCurrentTab(EXPERIMENTS_TAB_INDEX);
                break;
            case R.id.sheem_reconnect:
                mController.connectSheem();
                break;
            case R.id.voltmeter_reconnect:
                mController.connectVoltmeter();
                break;
            case R.id.ammeter_reconnect:
                mController.connectAmmeter();
                break;
            case R.id.pr_reconnect:
                mController.connectPR();
                break;
            case R.id.pm130_reconnect:
                mController.connectPM130();
                break;
            case R.id.ohmmeter_reconnect:
                mController.connectOhmmeter();
                break;
            case R.id.start_experiment:
                runNextExperiment();
                break;
            case R.id.protection_status:
                mControlPanelPresenter.protectionStatusClicked();
                break;
            case R.id.subject:
                mControlPanelPresenter.subjectClicked();
                break;
            case R.id.events:
                mControlPanelPresenter.eventsClicked();
                break;
            case R.id.devices_state:
                mControlPanelPresenter.devicesStateClicked();
                break;
            case R.id.data_transfer:
                mControlPanelPresenter.dataTransferClicked();
                break;
            case R.id.switcher:
                mControlPanelPresenter.switcherClicked();
                break;
            case R.id.experiment1_btn:
                mResultSelector.setSelection(0);
                break;
            case R.id.experiment2_btn:
                mResultSelector.setSelection(1);
                break;
            case R.id.experiment3_btn:
                mResultSelector.setSelection(2);
                break;
            case R.id.retry:
                switch (mModel.getCurrentExperiment()) {
                    case 1:
                    case 11:
                        if (mModel.isImpResponding()) {
                            mConnectionWarning.setVisibility(View.GONE);
                            runNextExperiment();
                        }
                        break;
                    case 2:
                        if (mModel.isVIUResponding()) {
                            mConnectionWarning.setVisibility(View.GONE);
                            runNextExperiment();
                        }
                        break;
                    case 3:
                        mConnectionWarning.setVisibility(View.GONE);
                        runNextExperiment();
                        break;
                }
                break;
            case R.id.save:
                if (fieldsAreFilled()) {
                    fillProtocol();
                    mController.saveProtocolInDB();
                    mModel.setNeedToSave(false);
                    Toast.makeText(this, "Сохранено", Toast.LENGTH_LONG).show();
                    showProtocols();
                } else {
                    Toast.makeText(this, "Заполните все поля", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.preview:
                Logging.preview(this, mModel.getProtocolForInteraction());
                break;
            case R.id.save_on_flash:
                Logging.saveFileOnFlashMassStorage(this, mModel.getProtocolForInteraction());
                break;
        }
    }

    private boolean fieldsAreFilled() {
        return !isEmpty(mPosition1Number.getText().toString()) &&
                !isEmpty(mPosition1FullName.getText().toString()) &&
                !isEmpty(mPosition2Number.getText().toString()) &&
                !isEmpty(mPosition2FullName.getText().toString());
    }

    public void fillProtocol() {
        mModel.savePosition1((String) mPosition1.getSelectedItem());
        mModel.savePosition1Number(mPosition1Number.getText().toString());
        mModel.savePosition1FullName(mPosition1FullName.getText().toString());
        mModel.savePosition2((String) mPosition2.getSelectedItem());
        mModel.savePosition2Number(mPosition2Number.getText().toString());
        mModel.savePosition2FullName(mPosition2FullName.getText().toString());
        mModel.saveDate(System.currentTimeMillis());
    }

    @SuppressLint("SetTextI18n")
    private void runNextExperiment() {
        switchTabState(mTabs, EXPERIMENTS_TAB_INDEX, false);
        hideDevicesStates();
        mTabHost.setCurrentTab(RESULT_TAB_INDEX);
        if (mModel.isFirstExperimentSelected()) {
            mModel.setCurrentExperiment(1);
            mResultSelector.setSelection(0);
            setViewAndChildrenVisibility(mSheem, View.VISIBLE);
            setViewAndChildrenVisibility(mVoltmeter, View.VISIBLE);
            setViewAndChildrenVisibility(mAmmeter, View.VISIBLE);
            mController.initTCDevices();
            initializeExperiment(1);
        } else if (mModel.isFirstTeachSelected()) {
            mModel.setCurrentExperiment(11);
            mResultSelector.setSelection(0);
            setViewAndChildrenVisibility(mSheem, View.VISIBLE);
            setViewAndChildrenVisibility(mVoltmeter, View.VISIBLE);
            setViewAndChildrenVisibility(mAmmeter, View.VISIBLE);
            mController.initTCDevices();
            initializeExperiment(11);
        } else if (mModel.isSecondExperimentSelected()) {
            mModel.setCurrentExperiment(2);
            mObjectUES.setText("");
            mObjectIES.setText("");
            mExperimentTimeES.setText("60");
            mResultES.setText("");
            mResultSelector.setSelection(1);
            setViewAndChildrenVisibility(mPr, View.VISIBLE);
            setViewAndChildrenVisibility(mPM130, View.VISIBLE);
            mController.initESDevices();
            initializeExperiment(2);
        } else if (mModel.isThirdExperimentSelected()) {
            mModel.setCurrentExperiment(3);
            mResultSelector.setSelection(2);
            setViewAndChildrenVisibility(mOhmmeter, View.VISIBLE);
            mController.initOHMDevices();
            initializeExperiment(3);
        } else {
            mModel.setCurrentExperiment(0);
            new AlertDialog.Builder(
                    this)
                    .setTitle("Все испытания закончены")
                    .setCancelable(false)
                    .setMessage("Сохранить результаты?")
                    .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            mModel.setNeedToSave(true);
                            mTabHost.setCurrentTab(PROTOCOL_TAB_INDEX);
                        }
                    })
                    .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            mTabHost.setCurrentTab(EXPERIMENTS_TAB_INDEX);
                        }
                    })
                    .create()
                    .show();
            switchTabState(mTabs, EXPERIMENTS_TAB_INDEX, true);
        }
    }

    private void initializeExperiment(final int experimentType) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (checkConnection(experimentType)) {
            mConnectionWarning.setVisibility(View.GONE);
            String message = "Подключите объект к X" + experimentType;
            if (experimentType == 11) {
                message = "Подключите эталонный объект к X" + 1;
            }
            new AlertDialog.Builder(
                    this)
                    .setTitle("Внимание")
                    .setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            mModel.removeExperiment(experimentType + "");
                            new ExperimentTask(experimentType).execute();
                        }
                    })
                    .create()
                    .show();
        } else {
            mConnectionWarning.setVisibility(View.VISIBLE);
        }
    }

    private boolean checkConnection(int experimentType) {
        boolean result = false;
        switch (experimentType) {
            case 1:
            case 11:
                if (mModel.isImpResponding()) {
                    result = true;
                }
                break;
            case 2:
                if (mModel.isVIUResponding()) {
                    result = true;
                }
                break;
            case 3:
                if (mModel.isOhmmeterResponding()) {
                    result = true;
                }
                break;
        }
        return result;
    }

    private void initializeSelectors() {
        setViewAndChildrenVisibility(mObjectsSelectorTitle, View.VISIBLE);
        setViewAndChildrenVisibility(mObjectsSelector, View.VISIBLE);
        setViewAndChildrenVisibility(mInductorsSelector, View.VISIBLE);
        setViewAndChildrenVisibility(mInductorsSelectorTitle, View.VISIBLE);
        setViewAndChildrenVisibility(mObjectEnter, View.VISIBLE);

        setSpinnerAdapter(this, mObjectsSelector, mController.getAllSubjectsFromDB());
        mObjectsSelector.post(new Runnable() {
            @Override
            public void run() {
                mObjectsSelector.performClick();
            }
        });
        mObjectsSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                List<Inductor> inductors = ((Subject) mObjectsSelector.getSelectedItem()).getInductors();
                setSpinnerAdapter(CoilsTestingFacilityActivity.this, mInductorsSelector,
                        inductors);
                if (inductors.size() == 1) {
                    setViewAndChildrenVisibility(mInductorsSelectorTitle, View.GONE);
                } else {
                    setViewAndChildrenVisibility(mInductorsSelectorTitle, View.VISIBLE);
                }
                if (!isFirstTime && (inductors.size() > 1)) {
                    mInductorsSelector.post(new Runnable() {
                        @Override
                        public void run() {
                            mInductorsSelector.performClick();
                        }
                    });
                } else {
                    isFirstTime = false;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class ExperimentTask extends AsyncTask<Void, Void, Integer> {
        int mExperimentType = 0;

        ExperimentTask(int experimentType) {
            mExperimentType = experimentType;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = 0;
            switch (mExperimentType) {
                case 1:
                    if (mModel.getSpecifiedITC() == -1) {
                        return 4;
                    }
                    result = mController.startTestTurnClosure();
                    break;
                case 11:
                    result = mController.startTeachingTurnClosure();
                    break;
                case 2:
                    result = mController.startTestElectricStrength();
                    break;
                case 3:
                    result = mController.startTestResistance();
                    break;
            }

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            switch (mExperimentType) {
                case 1:
                    if (result == OK_RESULT_TC) {
                        mModel.saveResultTC(true);
                        mResultTC.setText("Успешно");
                    } else if ((result == CONC_UP_RESULT_TC) || (result == BREAKDOWN_RESULT_TC)) {
                        mModel.saveResultTC(false);
                        mResultTC.setText("Неуспешно");
                    }
                    break;
                case 11:
                    break;
                case 2:
                    if (result == OK_RESULT_ES) {
                        mModel.saveResultES(true);
                        mResultES.setText("Успешно");
                    } else if (result == BREAKDOWN_RESULT_ES) {
                        mModel.saveResultES(false);
                        mObjectIES.setText(R.string.over5000);
                        mResultES.setText("Неуспешно");
                    } else if (result == CONC_UP_RESULT_ES) {
                        mModel.saveResultES(false);
                        mResultES.setText("Неуспешно");
                    }
                    break;
                case 3:
                    if (result == 0) {
                        mModel.saveResultR(true);
                        mResultR.setText("Успешно");
                    } else if (result == 1) {
                        mModel.saveResultR(false);
                        mResultR.setText("Неуспешно");
                    }
                    break;
            }
            showBottomDialog(mExperimentType, result);
            mController.diversifyDevices();
        }
    }

    private void showBottomDialog(int experimentType, int result) {
        String title = "";
        String message = "Желаете повторить ещё раз?";
        String appendix = "";
        switch (experimentType) {
            case 1:
                if (result == OK_RESULT_TC) {
                    title = "Проверка катушек на межвитковое замыкание успешно закончена";
                } else if (result == CONC_UP_RESULT_TC) {
                    title = "Не удалось выйти на заданное напряжение";
                } else if (result == BREAKDOWN_RESULT_TC) {
                    title = "Проверка на межвитковое замыкание неуспешно закончена. Произошёл пробой";
                } else if (result == AMPERAGE_IS_NOT_SPECIFIED) {
                    title = "Не задано I";
                    message = "Желаете провести обучение?";
                    appendix = "1";
                }
                break;
            case 11:
                if (result == 0) {
                    title = "Обучение успешно закончено";
                    message = "Желаете ли провести испытание?";
                    experimentType = 1;
                } else if ((result == 1) || (result == 2)) {
                    title = "Не удалось задать I";
                    message = "Желаете повторить ещё раз?";
                    appendix = "1";
                    experimentType = 1;
                }
                break;
            case 2:
                if (result == OK_RESULT_ES) {
                    title = "Проверка катушек на прочность изоляции успешно закончена";
                } else if (result == BREAKDOWN_RESULT_ES) {
                    title = "Проверка катушек на прочность неуспешно закончена. Произошёл пробой";
                } else if (result == CONC_UP_RESULT_ES) {
                    title = "Не удалось выйти на заданное напряжение";
                }
                break;
            case 3:
                if (result == 0) {
                    title = "Проверка величины активного сопротивления успешно закончена";
                } else if (result == 1) {
                    title = "Проверка величины активного сопротивления неуспешно закончена";
                }
                break;
        }
        final String finalExperimentType = appendix + experimentType;
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton("ДА", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        mModel.addExperiment(finalExperimentType);
                        runNextExperiment();
                    }
                })
                .setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        runNextExperiment();
                    }
                })
                .create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        @SuppressWarnings("ConstantConditions") WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        dialog.show();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean isControlPanelDisplayed() {
        return !mSwitcher.isChecked();
    }

    @Override
    public void hideControlPanel() {
        mMainTitle.setText("Состояние объекта испытания");
        mOptionSelectionLayout.setVisibility(View.GONE);
        mDevicesStatePanel.setVisibility(View.GONE);
        mTabHost.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayControlPanel() {
        mMainTitle.setText("Панель управления");
        mTabHost.setVisibility(View.GONE);
        mOptionSelectionLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayDevicesStatePanel() {
        mDevicesStatePanel.setVisibility(View.VISIBLE);
    }
}