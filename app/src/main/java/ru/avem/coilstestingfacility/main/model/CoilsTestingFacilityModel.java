package ru.avem.coilstestingfacility.main.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import ru.avem.coilstestingfacility.communication.devices.ammeter.AmmeterModel;
import ru.avem.coilstestingfacility.communication.devices.ohmmeter.OhmmeterModel;
import ru.avem.coilstestingfacility.communication.devices.pm130.PM130Model;
import ru.avem.coilstestingfacility.communication.devices.pr.PRModel;
import ru.avem.coilstestingfacility.communication.devices.sheem.SheemModel;
import ru.avem.coilstestingfacility.communication.devices.voltmeter.VoltmeterModel;
import ru.avem.coilstestingfacility.database.model.Inductor;
import ru.avem.coilstestingfacility.database.model.Protocol;
import ru.avem.coilstestingfacility.database.model.Subject;

import static ru.avem.coilstestingfacility.communication.devices.DeviceController.AMMETER_ID;
import static ru.avem.coilstestingfacility.communication.devices.DeviceController.OHMMETER_ID;
import static ru.avem.coilstestingfacility.communication.devices.DeviceController.PM130_ID;
import static ru.avem.coilstestingfacility.communication.devices.DeviceController.PR_ID;
import static ru.avem.coilstestingfacility.communication.devices.DeviceController.SHEEM_ID;
import static ru.avem.coilstestingfacility.communication.devices.DeviceController.VOLTMETER_ID;

public class CoilsTestingFacilityModel extends Observable implements Observer {
    public static final int EXPERIMENT_SELECTED_PARAM = 0;

    public static final int SHEEM_RESPONDING_PARAM = 1;
    public static final int VOLTMETER_RESPONDING_PARAM = 2;
    public static final int AMMETER_RESPONDING_PARAM = 3;
    private static final int IMP_RESPONDING_PARAM = 4;

    public static final int PR_RESPONDING_PARAM = 5;
    public static final int PM130_RESPONDING_PARAM = 6;
    private static final int VIU_RESPONDING_PARAM = 7;

    public static final int OHMMETER_RESPONDING_PARAM = 8;

    public static final int SPECIFIED_U_TC_PARAM = 9;
    public static final int OBJECT_U_TC_PARAM = 10;
    public static final int SPECIFIED_I_TC_PARAM = 11;
    public static final int OBJECT_I_TC_PARAM = 12;
    public static final int TIME_LEFT_TC_PARAM = 13;

    public static final int SPECIFIED_U_ES_PARAM = 14;
    public static final int OBJECT_U_ES_PARAM = 15;
    public static final int OBJECT_I_ES_PARAM = 16;
    public static final int TIME_LEFT_ES_PARAM = 17;

    public static final int SPECIFIED_R_PARAM = 18;
    public static final int OBJECT_R_PARAM = 19;

    private static final CoilsTestingFacilityModel instance = new CoilsTestingFacilityModel();

    private Protocol mProtocol;

    private List<String> mExperiments = new ArrayList<>();
    private int mCurrentExperiment;
    private boolean isNeedToSave;


    private boolean mSheemResponding;
    private boolean mVoltmeterResponding;
    private boolean mAmmeterResponding;
    private boolean mImpResponding;

    private boolean mPRResponding;
    private boolean mPM130Responding;
    private boolean mVIUResponding;

    private boolean mOhmmeterResponding;

    private float mOnlineUTC;
    private float mObjectUTC;
    private float mOnlineITC;
    private float mObjectITC;
    private boolean mExperimentEndTC;

    private float mOnlineUES;
    //    private float mObjectUES;
    private float mOnlineIES;
    //    private float mObjectIES;
    private boolean mOnlineConcUpES;
    private boolean mOnlineConcDownES;
    private boolean mOnlineAlarmES;
    private boolean mExperimentEndES;

    private float mOnlineR;
    private float mObjectR;

    private Protocol mProtocolForInteraction;

    private CoilsTestingFacilityModel() {
    }

    public static CoilsTestingFacilityModel getInstance() {
        return instance;
    }

    @Override
    public void update(Observable observable, Object values) {
        int modelId = (int) (((Object[]) values)[0]);
        int param = (int) (((Object[]) values)[1]);
        Object value = (((Object[]) values)[2]);

        switch (modelId) {
            case SHEEM_ID:
                switch (param) {
                    case SheemModel.RESPONDING_PARAM:
                        setSheemResponding((boolean) value);
                        break;
                }
                break;
            case VOLTMETER_ID:
                switch (param) {
                    case VoltmeterModel.RESPONDING_PARAM:
                        setVoltmeterResponding((boolean) value);
                        break;
                    case VoltmeterModel.U_PARAM:
                        setOnlineUTC((float) value);
                        break;
                }
                break;
            case AMMETER_ID:
                switch (param) {
                    case AmmeterModel.RESPONDING_PARAM:
                        setAmmeterResponding((boolean) value);
                        break;
                    case AmmeterModel.I_PARAM:
                        setOnlineITC((float) value);
                        break;
                }
                break;
            case PM130_ID:
                switch (param) {
                    case PM130Model.RESPONDING_PARAM:
                        setPM130Responding((boolean) value);
                        break;
                    case PM130Model.V1_PARAM:
                        break;
                    case PM130Model.V2_PARAM:
                        break;
                    case PM130Model.V3_PARAM:
                        setOnlineUES((float) value);
                        break;
                    case PM130Model.I1_PARAM:
                        break;
                    case PM130Model.I2_PARAM:
                        break;
                    case PM130Model.I3_PARAM:
                        setOnlineIES((float) value);
                        break;
                }
                break;
            case PR_ID:
                switch (param) {
                    case PRModel.RESPONDING_PARAM:
                        setPRResponding((boolean) value);
                        break;
                    case PRModel.ALARM_PARAM:
                        setOnlineAlarmES((boolean) value);
                        break;
                    case PRModel.AUTO_PARAM:
                        break;
                    case PRModel.UP_PARAM:
                        break;
                    case PRModel.DOWN_PARAM:
                        break;
                    case PRModel.CONC_UP_PARAM:
                        setOnlineConcUpES((boolean) value);
                        break;
                    case PRModel.CONC_DOWN_PARAM:
                        setOnlineConcDownES((boolean) value);
                        break;
                }
                break;
            case OHMMETER_ID:
                switch (param) {
                    case OhmmeterModel.RESPONDING_PARAM:
                        setOhmmeterResponding((boolean) value);
                        break;
                    case OhmmeterModel.MODE_PARAM:
                        break;
                    case OhmmeterModel.RESISTANCE_PARAM:
                        setOnlineR((float) value);
                        break;
                }
                break;
        }
    }

    private void setSheemResponding(boolean sheemResponding) {
        mSheemResponding = sheemResponding;
        notifySheemResponding(sheemResponding);
        checkImpResponding();
    }

    private void notifySheemResponding(boolean sheemResponding) {
        notice(SHEEM_RESPONDING_PARAM, sheemResponding);
    }

    private void checkImpResponding() {
        setImpResponding(mSheemResponding && mVoltmeterResponding && mAmmeterResponding);
    }

    private void setImpResponding(boolean impResponding) {
        mImpResponding = impResponding;
        notice(IMP_RESPONDING_PARAM, impResponding);
    }

    public boolean isImpResponding() {
        return mImpResponding;
    }

    private void setVoltmeterResponding(boolean voltmeterResponding) {
        mVoltmeterResponding = voltmeterResponding;
        notifyVoltmeterResponding(voltmeterResponding);
        checkImpResponding();
    }

    private void notifyVoltmeterResponding(boolean voltmeterResponding) {
        notice(VOLTMETER_RESPONDING_PARAM, voltmeterResponding);
    }

    private void setAmmeterResponding(boolean ammeterResponding) {
        mAmmeterResponding = ammeterResponding;
        notifyAmmeterResponding(ammeterResponding);
        checkImpResponding();
    }

    private void notifyAmmeterResponding(boolean ammeterResponding) {
        notice(AMMETER_RESPONDING_PARAM, ammeterResponding);
    }

    private void setPRResponding(boolean PRResponding) {
        mPRResponding = PRResponding;
        notifyPRResponding(PRResponding);
        checkVIUResponding();
    }

    private void notifyPRResponding(boolean PRResponding) {
        notice(PR_RESPONDING_PARAM, PRResponding);
    }

    private void checkVIUResponding() {
        setVIUResponding(mPRResponding && mPM130Responding);
    }

    public boolean isVIUResponding() {
        return mVIUResponding;
    }

    private void setVIUResponding(boolean VIUResponding) {
        mVIUResponding = VIUResponding;
        notice(VIU_RESPONDING_PARAM, VIUResponding);
    }

    private void setPM130Responding(boolean PM130Responding) {
        mPM130Responding = PM130Responding;
        notifyPM130Responding(PM130Responding);
        checkVIUResponding();
    }

    private void notifyPM130Responding(boolean PM130Responding) {
        notice(PM130_RESPONDING_PARAM, PM130Responding);
    }

    public boolean isOhmmeterResponding() {
        return mOhmmeterResponding;
    }

    private void setOhmmeterResponding(boolean ohmmeterResponding) {
        mOhmmeterResponding = ohmmeterResponding;
        notifyOhmmeterResponding(ohmmeterResponding);
    }

    private void notifyOhmmeterResponding(boolean ohmmeterResponding) {
        notice(OHMMETER_RESPONDING_PARAM, ohmmeterResponding);
    }


    public Protocol getProtocol() {
        return mProtocol;
    }

    public void setProtocol(Protocol protocol) {
        mProtocol = protocol;
    }

    public long getProtocolId() {
        return mProtocol.getId();
    }

    public void saveProtocolId(long protocolId) {
        mProtocol.setId(protocolId);
    }

    public String getSubjectName() {
        return mProtocol.getSubjectName();
    }

    public void saveSubjectName(Subject subject) {
        mProtocol.setSubjectName(subject.getName());
    }

    public String getInductorName() {
        return mProtocol.getInductorName();
    }

    public void saveInductor(Inductor inductor) {
        mProtocol.setInductorName(inductor.getName());
        mProtocol.setSpecifiedUTC(inductor.getSpecifiedUTC());
        notifySpecifiedUTC(inductor.getSpecifiedUTC());
        mProtocol.setSpecifiedUTCMinus(inductor.getSpecifiedUTCMinus());
        mProtocol.setSpecifiedUTCPlus(inductor.getSpecifiedUTCPlus());
        mProtocol.setSpecifiedITC(inductor.getSpecifiedITC());
        notifySpecifiedITC(inductor.getSpecifiedITC());
        mProtocol.setSpecifiedITCMinus(inductor.getSpecifiedITCMinus());
        mProtocol.setSpecifiedITCPlus(inductor.getSpecifiedITCPlus());
        mProtocol.setSpecifiedUES(inductor.getSpecifiedUES());
        notifySpecifiedUES(inductor.getSpecifiedUES());
        mProtocol.setSpecifiedUESMinus(inductor.getSpecifiedUESMinus());
        mProtocol.setSpecifiedUESPlus(inductor.getSpecifiedUESPlus());
        mProtocol.setSpecifiedR(inductor.getSpecifiedR());
        notifySpecifiedR(inductor.getSpecifiedR());
        mProtocol.setSpecifiedRMinus(inductor.getSpecifiedRMinus());
        mProtocol.setSpecifiedRPlus(inductor.getSpecifiedRPlus());
        mProtocol.setExperimentTime(inductor.getExperimentTime());
    }

    public void fillSpecifiedFields() {
        notifySpecifiedUTC(mProtocol.getSpecifiedUTC());
        notifySpecifiedITC(mProtocol.getSpecifiedITC());
        notifySpecifiedUES(mProtocol.getSpecifiedUES());
        notifySpecifiedR(mProtocol.getSpecifiedR());
    }

    public double getSpecifiedUTC() {
        return mProtocol.getSpecifiedUTC();
    }

    public double getSpecifiedUTCMinus() {
        return mProtocol.getSpecifiedUTCMinus();
    }

    public double getSpecifiedUTCPlus() {
        return mProtocol.getSpecifiedUTCPlus();
    }

    public double getSpecifiedITC() {
        return mProtocol.getSpecifiedITC();
    }

    public void setSpecifiedITC(double specifiedITC) {
        mProtocol.setSpecifiedITC(specifiedITC);
        notifySpecifiedITC(specifiedITC);
    }

    public void setSpecifiedITCMinus(double specifiedITCMinus) {
        mProtocol.setSpecifiedITCMinus(specifiedITCMinus);
    }

    public void setSpecifiedITCPlus(double specifiedITCPlus) {
        mProtocol.setSpecifiedITCPlus(specifiedITCPlus);
    }

    private void notifySpecifiedUTC(double specifiedUTC) {
        notice(SPECIFIED_U_TC_PARAM, specifiedUTC);
    }

    private void notifySpecifiedITC(double specifiedITC) {
        notice(SPECIFIED_I_TC_PARAM, specifiedITC);
    }

    private void notifySpecifiedUES(double specifiedUES) {
        notice(SPECIFIED_U_ES_PARAM, specifiedUES);
    }

    private void notifySpecifiedR(double specifiedR) {
        notice(SPECIFIED_R_PARAM, specifiedR);
    }

    public double getSpecifiedUES() {
        return mProtocol.getSpecifiedUES();
    }

    public double getSpecifiedRMinus() {
        return mProtocol.getSpecifiedRMinus();
    }

    public double getSpecifiedRPlus() {
        return mProtocol.getSpecifiedRPlus();
    }

    public int getExperimentTime() {
        return mProtocol.getExperimentTime();
    }

    private void saveObjectUTC(double objectUTC) {
        mProtocol.setObjectUTC(objectUTC);
    }

    private void saveObjectITC(double objectITC) {
        mProtocol.setObjectITC(objectITC);
    }

    public void saveResultTC(boolean resultTC) {
        mProtocol.setResultTC(resultTC);
    }

    private void saveObjectUES(double objectUES) {
        mProtocol.setObjectUES(objectUES);
    }

    private void saveObjectIES(double objectIES) {
        mProtocol.setObjectIES(objectIES);
    }

    public void saveResultES(boolean resultES) {
        mProtocol.setResultES(resultES);
    }

    private void saveObjectR(double objectR) {
        mProtocol.setObjectR(objectR);
    }

    public void saveResultR(boolean resultR) {
        mProtocol.setResultR(resultR);
    }

    public void savePosition1(String position1) {
        mProtocol.setPosition1(position1);
    }

    public void savePosition1Number(String position1Number) {
        mProtocol.setPosition1Number(position1Number);
    }

    public void savePosition1FullName(String position1FullName) {
        mProtocol.setPosition1FullName(position1FullName);
    }

    public void savePosition2(String position2) {
        mProtocol.setPosition2(position2);
    }

    public void savePosition2Number(String position2Number) {
        mProtocol.setPosition2Number(position2Number);
    }

    public void savePosition2FullName(String position2FullName) {
        mProtocol.setPosition2FullName(position2FullName);
    }

    public void saveDate(long date) {
        mProtocol.setDate(date);
    }


    public void addExperiment(String number) {
        mExperiments.add(number);
        checkAvailabilityExperiments();
    }

    private void checkAvailabilityExperiments() {
        notice(EXPERIMENT_SELECTED_PARAM, mExperiments.size() > 0);
    }

    public void removeExperiment(String number) {
        mExperiments.remove(number);
        checkAvailabilityExperiments();
    }

    public void removeAllExperiments() {
        mExperiments.clear();
    }

    public int getCurrentExperiment() {
        return mCurrentExperiment;
    }

    public void setCurrentExperiment(int currentExperiment) {
        mCurrentExperiment = currentExperiment;
    }

    public boolean isFirstExperimentSelected() {
        return mExperiments.contains("1");
    }

    public boolean isFirstTeachSelected() {
        return mExperiments.contains("11");
    }

    public boolean isSecondExperimentSelected() {
        return mExperiments.contains("2");
    }

    public boolean isThirdExperimentSelected() {
        return mExperiments.contains("3");
    }

    public boolean isNeedToSave() {
        return isNeedToSave;
    }

    public void setNeedToSave(boolean needToSave) {
        isNeedToSave = needToSave;
    }

    public float getOnlineUTC() {
        return mOnlineUTC;
    }

    private void setOnlineUTC(float onlineUTC) {
        if (onlineUTC < 4000) {
            mOnlineUTC = onlineUTC;
            if (!isExperimentEndTC()) {
                setObjectUTC(onlineUTC);
            }
        }
    }

    public float getObjectUTC() {
        return mObjectUTC;
    }

    private void setObjectUTC(float objectUTC) {
        mObjectUTC = objectUTC;
        notice(OBJECT_U_TC_PARAM, objectUTC);
    }

    public float getOnlineITC() {
        return mOnlineITC;
    }

    private void setOnlineITC(float onlineITC) {
        mOnlineITC = onlineITC;
        if (!isExperimentEndTC()) {
            setObjectITC(onlineITC);
        }
    }

    public float getObjectITC() {
        return mObjectITC;
    }

    private void setObjectITC(float objectITC) {
        mObjectITC = objectITC;
        notice(OBJECT_I_TC_PARAM, objectITC);
    }

    private boolean isExperimentEndTC() {
        return mExperimentEndTC;
    }

    public void setExperimentEndTC(boolean experimentEndTC) {
        mExperimentEndTC = experimentEndTC;
    }

    public void saveTCResult() {
        saveObjectUTC(mObjectUTC);
        saveObjectITC(mObjectITC);
//        setObjectUTC(mOnlineUTC);
//        setObjectITC(mOnlineITC);
        setExperimentEndTC(true);
    }


    public float getOnlineUES() {
        return mOnlineUES;
    }

    private void setOnlineUES(float onlineUES) {
        mOnlineUES = onlineUES;
        if (!isExperimentEndES()) {
            saveObjectUES(onlineUES);
            notice(OBJECT_U_ES_PARAM, onlineUES);
        }
    }

//    public float getOnlineIES() {
//        return mOnlineIES;
//    }

    private void setOnlineIES(float onlineIES) {
        mOnlineIES = onlineIES;
        if (!isExperimentEndES()) {
            saveObjectIES(onlineIES);
            notice(OBJECT_I_ES_PARAM, onlineIES);
        }
    }

    public boolean isOnlineConcUpES() {
        return mOnlineConcUpES;
    }

    private void setOnlineConcUpES(boolean onlineConcUpES) {
        mOnlineConcUpES = onlineConcUpES;
    }

    public boolean isOnlineConcDownES() {
        return mOnlineConcDownES;
    }

    private void setOnlineConcDownES(boolean onlineConcDownES) {
        mOnlineConcDownES = onlineConcDownES;
    }

    public boolean isOnlineAlarmES() {
        return mOnlineAlarmES;
    }

    private void setOnlineAlarmES(boolean onlineAlarmES) {
        mOnlineAlarmES = onlineAlarmES;
    }

    public void setTimeLeftES(int timeLeftES) {
        notice(TIME_LEFT_ES_PARAM, timeLeftES);
    }

    public void setTimeLeftTC(int timeLeftTC) {
        notice(TIME_LEFT_TC_PARAM, timeLeftTC);
    }

    private boolean isExperimentEndES() {
        return mExperimentEndES;
    }

    public void setExperimentEndES(boolean experimentEndES) {
        mExperimentEndES = experimentEndES;
    }


    public float getOnlineR() {
        return mOnlineR;
    }

    private void setOnlineR(float onlineR) {
        mOnlineR = onlineR;
    }


    public float getObjectR() {
        return mObjectR;
    }

    public void setObjectR(float objectR) {
        mObjectR = objectR;
        saveObjectR(objectR);
        notice(OBJECT_R_PARAM, objectR);
    }


    public Protocol getProtocolForInteraction() {
        return mProtocolForInteraction;
    }

    public void setProtocolForInteraction(Protocol protocolForInteraction) {
        mProtocolForInteraction = protocolForInteraction;
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{param, value});
    }
}