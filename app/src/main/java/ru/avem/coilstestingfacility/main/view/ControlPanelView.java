package ru.avem.coilstestingfacility.main.view;

public interface ControlPanelView {
    boolean isControlPanelDisplayed();

    void hideControlPanel();

    void displayControlPanel();

    void displayDevicesStatePanel();
}
